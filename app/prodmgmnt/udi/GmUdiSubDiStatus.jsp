<%
/**********************************************************************************
 * File		 		: GmUdisubDiStatus.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Manikandan
************************************************************************************/
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtUdiSubDiStatus" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\udi\GmUdiSubDiStatus.jsp -->
<fmtUdiSubDiStatus:setLocale value="<%=strLocale%>"/>
<fmtUdiSubDiStatus:setBundle basename="properties.labels.prodmgmnt.udi.GmUdiSubDiStatus"/>

<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@page import="java.util.Date"%>
<bean:define id="partNum" name="frmUdiSubReportDashboard" property="partNum" type="java.lang.String"></bean:define>
<bean:define id="alProjectId" name="frmUdiSubReportDashboard" property="alProjectId" type="java.util.ArrayList"> </bean:define>
<bean:define id="gridXmlData" name="frmUdiSubReportDashboard" property="gridXmlData" type="java.lang.String"> </bean:define>

<% 
	String strProdMgmntUdiJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_UDI");
	String strWikiTitle = GmCommonClass.getWikiTitle("UDI_STATUS");
	String strSearch = GmCommonClass.parseNull((String) request.getAttribute("hSearch"));

%>

<HTML>
<HEAD>
<TITLE>Globus Medical: UDI WIP DashBoard</TITLE>
<link rel="stylesheet" type="text/css"  href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript"   src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript"	src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript"	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"	src="<%=strProdMgmntUdiJsPath%>/GmUdiSubDiStatus.js"></script>
<script language="JavaScript"	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript"	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script>
var objGridData;
objGridData = '<%=gridXmlData%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
	<html:form action="/gmUdiReport.do?method=udiDiStatus">
		<html:hidden property="strOpt" value=""
			name="frmUdiSubReportDashboard" />
		<input type="hidden" name="hSearch" value="<%=strSearch%>">

		<input type="hidden" name="diNumber" value="">
		<input type="hidden" name="partNum" value="">
		<table border="0" width="100%" class="DtTable1100" cellspacing="0"
			cellpadding="0">

			<tr>
				<td height="25" class="RightDashBoardHeader" colspan="7">&nbsp;<fmtUdiSubDiStatus:message key="LBL_GTIN_DI_STATUS"/></td>
				<td height="25" class="RightDashBoardHeader"><fmtUdiSubDiStatus:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="RightTableCaption" height="30" colspan="1" align="right">
				<fmtUdiSubDiStatus:message key="LBL_GTIN_DI" var="varGTINDI"/><gmjsp:label type="RegularText" SFLblControlName="${varGTINDI}:"
						td="false" />
				</td>
				<td><textarea name="diNumberText" class="HtmlTextArea" rows="10" cols="30" onfocus="changeBgColor(this,'#AACCE8');"
					styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"><bean:write name="frmUdiSubReportDashboard" property="diNumberText" /></textarea></td>
				<td class="RightTableCaption" height="30" colspan="1" align="right"><select
					name="Cbo_Search" class="RightText" tabindex="2"
					onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');">
						<option value="0"><fmtUdiSubDiStatus:message key="LBL_CHOOSE_ONE"/>
						<option value="LIT"><fmtUdiSubDiStatus:message key="LBL_LITERAL"/>
						<option value="LIKEPRE"><fmtUdiSubDiStatus:message key="LBL_LIKE_PREFIX"/>
						<option value="LIKESUF"><fmtUdiSubDiStatus:message key="LBL_LIKE_SUFFIX"/>
				</select></td>
				<td class="RightTableCaption" height="30" colspan="1" align="right">
					<fmtUdiSubDiStatus:message key="LBL_PART" var="varPart"/><gmjsp:label type="RegularText" SFLblControlName="${varPart}:"
						td="false" />
				</td>
				<td><textarea name="partNumberText" class="HtmlTextArea" rows="10" cols="30" onfocus="changeBgColor(this,'#AACCE8');" 
					styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"><bean:write name="frmUdiSubReportDashboard" property="partNumberText" /></textarea></td>

			</tr>
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr class="Shade">

				<td class="RightTableCaption" height="30" colspan="1" align="right">
				<fmtUdiSubDiStatus:message key="LBL_PROJECT_ID" var="varProjectID"/>	<gmjsp:label type="RegularText" SFLblControlName="${varProjectID}:"
						td="false" />
				</td>
				<td class="RightTableCaption" height="30" colspan="2">&nbsp;<gmjsp:dropdown
						controlName="projectId" SFFormName="frmUdiSubReportDashboard"
						SFSeletedValue="projectId" SFValue="alProjectId" codeId="ID"
						codeName="IDNAME" defaultValue="[Choose One]" tabIndex="4"
						width="300" /></td>
						
				<td class="RightTableCaption" height="30" colspan="1" align="right">
				<fmtUdiSubDiStatus:message key="LBL_INTERNAL_STATUS" var="varInternalStatus"/>	<gmjsp:label type="RegularText" SFLblControlName="${varInternalStatus}:"
						td="false" />
				</td>
				<td class="RightTableCaption" height="30" colspan="4">&nbsp;<gmjsp:dropdown
						controlName="internalSts" SFFormName="frmUdiSubReportDashboard"
						SFSeletedValue="internalSts" SFValue="alInternalStatus" codeId="CODEID"
						codeName="CODENM" defaultValue="[Choose One]" tabIndex="5"
						width="300" /></td>		
				
			</tr>
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" height="30" colspan="1" align="right">
				<fmtUdiSubDiStatus:message key="LBL_EXTERNAL_STATUS" var="varExternalStatus"/>	<gmjsp:label type="RegularText" SFLblControlName="${varExternalStatus}:"
						td="false" />
				</td>
				<td class="RightTableCaption" height="30" colspan="2">&nbsp;<gmjsp:dropdown
						controlName="externalSts" SFFormName="frmUdiSubReportDashboard"
						SFSeletedValue="externalSts" SFValue="alExternalStatus" codeId="CODEID"
						codeName="CODENM" defaultValue="[Choose One]" tabIndex="6"
						width="300" /></td>		
						
				<td height="30" colspan="5" align="center"><fmtUdiSubDiStatus:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load"
						value="${varLoad}" style="width: 5em; height: 23px;" gmClass="Button"
						onClick="javascript:fnLoad();" tabindex="7" buttonType="Load" /></td>
			
			</tr>

			<%
				if (gridXmlData.indexOf("cell") != -1) {
			%>

			<tr>
				<td colspan="8">
					<div id="distatus" style="height: 500px; width: 1090px"></div>
					<div id="pagingArea" style="width: 1090px"></div>
				</td>
			</tr>

			<tr>
				<td colspan="8" align="center" height="25" width="100%">
					<div class='exportlinks'>
						<fmtUdiSubDiStatus:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' /> <a href="#"
							onclick="javascript:fnExportExcel();"> <fmtUdiSubDiStatus:message key="DIV_EXCEL"/> </a>
				</td>
			</tr>
			<%
				} else if (!gridXmlData.equals("")) {
			%>
			<tr>
				<td colspan="9" height="1" class="LLine"></td>
			</tr>
			<tr>
			<tr>
				<td colspan="9" align="center" class="RightText">
<fmtUdiSubDiStatus:message key="LBL_NO_DATA"/></td>
			</tr>
			</tr>
			<%
				} 
			%>
		</table>

	</html:form>

	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
