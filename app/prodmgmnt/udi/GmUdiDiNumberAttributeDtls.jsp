
<%
	/**********************************************************************************
	 * File		 		: GmUdiDiNumberAttributeDtls.jsp
	 * Desc		 		: This screen is used show UDI DI Attribute details
	 * Version	 		: 1.0
	 * author			: rdinesh
	 ************************************************************************************/
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtUdiDiNumberAttributeDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\udi\GmUdiDiNumberAttributeDtls.jsp -->
<fmtUdiDiNumberAttributeDtls:setLocale value="<%=strLocale%>"/>
<fmtUdiDiNumberAttributeDtls:setBundle basename="properties.labels.prodmgmnt.udi.GmUdiDiNumberAttributeDtls"/>

<bean:define id="gridData" name="frmUdiSubReportDashboard" property="gridXmlData" type="java.lang.String"></bean:define>
<bean:define id="diNumber" name="frmUdiSubReportDashboard" property="diNumber" type="java.lang.String"></bean:define>
<bean:define id="gridErrorXmlData" name="frmUdiSubReportDashboard" property="gridErrorXmlData" type="java.lang.String"></bean:define>

<%
	String strProdMgmntUdiJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_UDI");
	String strWikiTitle = GmCommonClass.getWikiTitle("UDI_DI_NUM_ATTR_DTLS");
%>

<html>
<head>
<title>Globus Medical: UDI Attribute Details</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strProdMgmntUdiJsPath%>/GmUdiSubErrorReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<script>
var objGridData1 = '<%=gridData%>';
var check_rowId = '';
var diNum_rowId = '';
	function fnOnPageLoad1() {
		gridObj1 = initGridData1('udiAttributeDtls', objGridData1);
		fnOnPageLoad ();
	}
	function fnLoad(){
		//fnStartProgress('Y');
		document.frmUdiSubReportDashboard.strOpt.value = "Load";
		document.frmUdiSubReportDashboard.submit();
	}
	function initGridData1(divRef,gridData)
	{
		var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
		gObj.init();	
		gObj.loadXMLString(gridData);
		gObj.attachHeader("#text_filter,#text_filter,#select_filter,#text_filter,#select_filter");
		return gObj;	
	}

	//Function to export excel
	function fnExportExcel() {
		
	gridObj1.toExcel('/phpapp/excel/generate.php');
	
	}
	function fnDhtmlxClose(){
		CloseDhtmlxWindow();
		return false;
	}
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad1();">
	<html:form action="/gmUdiReport.do?method=udiDiNumberAttributeDtls">
		<html:hidden property="strOpt"/>
		<html:hidden property="diNumber"/>
		<table class="DtTable800" border="0" cellpadding="0" cellspacing="0">

			<tr>
				<td class="RightDashBoardHeader" height="25" colspan="7"><fmtUdiDiNumberAttributeDtls:message key="LBL_UDI_ATTRIBUTE_DETAILS"/>
					</td>
				<td height="25" align="right" class=RightDashBoardHeader>
				 <fmtUdiDiNumberAttributeDtls:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' 
					src='<%=strImagePath%>/help.gif' title='${varHelp}'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr>
				<td colspan="8"><div id="udiAttributeDtls" style="width: 1100px; height: 200px"></div>
			</tr>
			<tr>
			<td colspan="8">
			<jsp:include page="/prodmgmnt/udi/GmUdiSubErrorReport.jsp">
			<jsp:param name="gridErrorXmlData" value="<%=gridErrorXmlData %>"/>
			<jsp:param name="screenName" value="DIATTR" />
			</jsp:include>
			</td>
			</tr>
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr>
				<td colspan="8" align="center" height="25" width="100%">
					<div class='exportlinks'>
						<fmtUdiDiNumberAttributeDtls:message key="DIV_EXPORT_OPT"/>:<img src='img/ico_file_excel.png' /> <a href="#"
							onclick="fnExportExcel();"><fmtUdiDiNumberAttributeDtls:message key="DIV_EXCEL"/> </a></div>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="8" height="35">
				<fmtUdiDiNumberAttributeDtls:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="${varClose}" gmClass="Button" onClick="fnDhtmlxClose();" tabindex="8" buttonType="Load" />&nbsp;
				</td>
			</tr>
			
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>
