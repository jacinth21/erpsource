<%
/**********************************************************************************
 * File		 		: GmUdiMissingAttributeRpt.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Manikandan
************************************************************************************/
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtUdiMissingAttributeRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\udi\GmUdiMissingAttributeRpt.jsp -->
<fmtUdiMissingAttributeRpt:setLocale value="<%=strLocale%>"/>
<fmtUdiMissingAttributeRpt:setBundle basename="properties.labels.prodmgmnt.udi.GmUdiMissingAttributeRpt"/>

<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@page import="java.util.Date"%>
<bean:define id="partNum" name="frmUdiSubReportDashboard" property="partNum" type="java.lang.String"></bean:define>
<bean:define id="gridXmlData" name="frmUdiSubReportDashboard" property="gridXmlData" type="java.lang.String"> </bean:define>
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("MISSING_ATTRIBUTE_REPORT");

	String strProdMgmntUdiJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_UDI");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical:Missing Attribute Report </TITLE>
<link rel = "stylesheet" type = "text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strProdMgmntUdiJsPath%>/GmUdiMissingAttributeRpt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script>
var objGridData;
objGridData = '<%=gridXmlData%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"  onload="fnOnLoad();">
	<html:form  action="/gmUdiReport.do?method=udiMissingAttributeDetail">
	<html:hidden property="strOpt"  value="" name="frmUdiSubDashboard"/>
	
		<table border="0"  width="100%" class="DtTable1100" cellspacing="0" cellpadding="0">
		
				<tr >
					<td height="25" class="RightDashBoardHeader" colspan="7">&nbsp;
<fmtUdiMissingAttributeRpt:message key="LBL_MISSING_ATTRIBUTE_REPORT"/></td>
				
				<td  height="25" class="RightDashBoardHeader">
						<fmtUdiMissingAttributeRpt:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
				</tr>
				
				
				 <%if(gridXmlData.indexOf("cell")!=-1){ %>
			   
				<tr>
					<td colspan="8">
						<div  id="MissingAttributeReport" style="height: 400px; width: 1100px"></div>
						
					</td>
				</tr>	
				<tr>
				<td colspan="7" align="center" height="25" width="100%">
					<div class='exportlinks'><fmtUdiMissingAttributeRpt:message key="DIV_EXPORT_OPT"/>:<img src='img/ico_file_excel.png' /> <a href="#" onclick="javascript:fnExportExcel();"><fmtUdiMissingAttributeRpt:message key="DIV_EXCEL"/> </a>
				</td>
			</tr>					
			<%}else{%>
				<tr>
					<td colspan="9" height="1" class="LLine"></td></tr>
				<tr>
					<tr ><td colspan="9" align="center" class="RightText" ><fmtUdiMissingAttributeRpt:message key="LBL_NO_DATA"/></td></tr>
				</tr> 
			<%}%>
			
			<tr>
				<td align="center" colspan="7" height="35">
				<fmtUdiMissingAttributeRpt:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="${Close}" gmClass="Button" onClick="fnClose();" tabindex="8" buttonType="Load" />&nbsp;
				</td>
			</tr>
		</table>

	</html:form>

		<%@ include file="/common/GmFooter.inc"%>
	</BODY>
</HTML>
