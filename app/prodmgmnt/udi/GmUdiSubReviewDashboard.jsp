
<%
	/**********************************************************************************
	 * File		 		: GmUdiSubReviewDashboard.jsp
	 * Desc		 		: This screen is used show UDI review dashboard
	 * Version	 		: 1.0
	 * author			: rdinesh
	 ************************************************************************************/
%>
<!-- prodmgmnt\udi\GmUdiSubReviewDashboard.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtUdiSubReviewDashboard" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtUdiSubReviewDashboard:setLocale value="<%=strLocale%>" />
<fmtUdiSubReviewDashboard:setBundle basename="properties.labels.prodmgmnt.udi.GmUdiSubReviewDashboard" />
<bean:define id="gridData" name="frmUdiSubDashboard"
	property="gridXmlData" type="java.lang.String">
</bean:define>
<bean:define id="UDIacessFl" name="frmUdiSubDashboard"
	property="UDIacessFl" type="java.lang.String">
</bean:define>


<%

	String strProdMgmntUdiJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_UDI");
	String strWikiTitle = GmCommonClass.getWikiTitle("UDI_REVIEW_DASH");
	String strSearch = GmCommonClass.parseNull((String) request
			.getAttribute("hSearch"));
	String strBtnDisable = "true";
	if (UDIacessFl.equals("Y")) {
		strBtnDisable = "false";
	}
%>

<html>
<head>
<title>Globus Medical: UDI Review Dashboard</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">


<script language="JavaScript"
	src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"
	src="<%=strProdMgmntUdiJsPath%>/GmUdiSubReviewDashboard.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
var objGridData = '<%=gridData%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad()">
	<html:form action="/gmUdiSubDashboard.do?method=udiReviewDashboard">
		<html:hidden property="strOpt" value="frmUdiSubDashboard" />
		<html:hidden property="inputStr" value="frmUdiSubDashboard" />
		<input type="hidden" name="hSearch" value="<%=strSearch%>">
		<input type="hidden" name="diNumber" value="">
		<input type="hidden" name="partNum" value="">
		<table class="DtTable1100" border="0" cellpadding="0" cellspacing="0">

			<tr>
				<td class="RightDashBoardHeader" height="25" colspan="7"><fmtUdiSubReviewDashboard:message
						key="LBL_UDI_REVIEW_DASHBOARD" /></td>
				<td height="25" align="right" class=RightDashBoardHeader>
				<fmtUdiSubReviewDashboard:message key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit' align="right"
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="RightTableCaption" height="30" colspan="1" align="right">
					<fmtUdiSubReviewDashboard:message key="LBL_GTIN_DI"
						var="varGTINDI" />
					<gmjsp:label type="RegularText"
						SFLblControlName="${varGTINDI}:" td="false" />&nbsp;
				</td>
				<td><textarea name="diNumberText" class="HtmlTextArea" rows="10" cols="30" onfocus="changeBgColor(this,'#AACCE8');"
					styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"><bean:write name="frmUdiSubDashboard" property="diNumberText" /></textarea></td>
				<td class="RightTableCaption" height="30" colspan="1" align="right"><select
					name="Cbo_Search" class="RightText" tabindex="2"
					onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');">
						<option value="0"><fmtUdiSubReviewDashboard:message key="LBL_CHOOSE_ONE"/>
						<option value="LIT"><fmtUdiSubReviewDashboard:message key="LBL_LITERAL"/>
						<option value="LIKEPRE"><fmtUdiSubReviewDashboard:message key="LBL_LIKE_PREFIX"/>
						<option value="LIKESUF"><fmtUdiSubReviewDashboard:message key="LBL_LIKE_SUFFIX"/>
				</select></td>
				<td class="RightTableCaption" height="30" colspan="1" align="right">
					<fmtUdiSubReviewDashboard:message key="LBL_PART" var="varPart" />
					<gmjsp:label type="RegularText" SFLblControlName="${varPart}:"
						td="false" />&nbsp;
				</td>
				<td><textarea name="partNumberText" class="HtmlTextArea" rows="10" cols="30" onfocus="changeBgColor(this,'#AACCE8');" 
				     styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"><bean:write name="frmUdiSubDashboard" property="partNumberText" /></textarea></td>
			</tr>
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<td class="RightTableCaption" height="25" colspan="1" align="right">
					<fmtUdiSubReviewDashboard:message key="LBL_PROJECT_ID"
						var="varProjectID" />
					<gmjsp:label type="RegularText"
						SFLblControlName="${varProjectID}:" td="false" />&nbsp;
				</td>
				<td class="RightTableCaption" height="25" colspan="2"><gmjsp:dropdown
						controlName="projectId" SFFormName="frmUdiSubDashboard"
						SFSeletedValue="projectId" SFValue="alProjectId" codeId="ID"
						codeName="IDNAME" defaultValue="[Choose One]" tabIndex="4"
						width="300" /></td>
				<td height="25" colspan="5"><fmtUdiSubReviewDashboard:message
						key="BTN_LOAD" var="varLoad" />
					<gmjsp:button name="BTN_Load" value="${varLoad}"
						style="width: 5em; height: 23px;" gmClass="Button"
						onClick="javascript:fnLoad();" tabindex="5" buttonType="Load" /></td>
			</tr>
			<%
				if (gridData.indexOf("cell") != -1) {
			%>
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr>
				<td colspan="8"><div id="dataGridDiv" width="1100px"
						height="460px"></div>
					<div id="pagingArea" width="1100px"></div></td>
			</tr>
			<tr>
				<td colspan="8" align="center"><fmtUdiSubReviewDashboard:message
						key="BTN_TRANSFET_TO_GDSN" var="varTransfertoGDSN" />
					<gmjsp:button value="${varTransfertoGDSN}"
						gmClass="Button" name="Submit" style="width: 10em"
						disabled="<%=strBtnDisable %>" buttonType="Save"
						onClick="fnTransferToGDSN();" /></td>
			</tr>
			<tr>
				<td align="center" colspan="7"><br>
					<div class='exportlinks'>
						<fmtUdiSubReviewDashboard:message key="DIV_EXPORT_OPT" />
						: <img src='img/ico_file_excel.png' />&nbsp; <a
							href="#" onclick="fnExport();"> <fmtUdiSubReviewDashboard:message
								key="DIV_EXCEL" />
						</a>
					</div></td>
			<tr>
				<%
					} else if (!gridData.equals("")) {
				%>
			
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr>
				<td colspan="8" align="center" class="RightText"><fmtUdiSubReviewDashboard:message
						key="LBL_NO_DATA" /></td>
			</tr>
			<%
				}
			%>

		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>

