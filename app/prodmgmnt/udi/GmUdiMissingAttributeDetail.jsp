<%
/**********************************************************************************
 * File		 		: GmUdiSubWIPDashboard.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Manikandan
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtUdiMissingAttributeDetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\udi\GmUdiMissingAttributeDetail.jsp -->
<fmtUdiMissingAttributeDetail:setLocale value="<%=strLocale%>"/>
<fmtUdiMissingAttributeDetail:setBundle basename="properties.labels.prodmgmnt.udi.GmUdiMissingAttributeDetail"/>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@page import="java.util.Date"%>
<bean:define id="partNum" name="frmUdiSubReportDashboard" property="partNum" type="java.lang.String"></bean:define>
<bean:define id="gridXmlData" name="frmUdiSubReportDashboard" property="gridXmlData" type="java.lang.String"> </bean:define>
<% 
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");

	String strProdMgmntUdiJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_UDI");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: WIP Missing Attribute Detail </TITLE>
<link rel = "stylesheet" type = "text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strProdMgmntUdiJsPath%>/GmUdiMissingAttributeDetail.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>


<script>
var objGridData;
objGridData = '<%=gridXmlData%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"  onload="fnOnLoad();">
	<html:form  action="/gmUdiReport.do?method=udiDiStatus">
	<html:hidden property="strOpt"  value="" name="frmUdiSubDashboard"/>
	
		<table border="0"  width="100%" class="DtTable700" cellspacing="0" cellpadding="0">
		
				<tr >
					<td height="25" class="RightDashBoardHeader" colspan="7">&nbsp;
<fmtUdiMissingAttributeDetail:message key="LBL_MISSING_ATTRIBUTE"/></td>
				</tr>
				
				
				 <%if(gridXmlData.indexOf("cell")!=-1){ %>
			   
				<tr>
					<td colspan="8">
						<div  id="MissingAttribute" style="height: 500px; width: 700px"></div>
						
					</td>
				</tr>						
			<%}else{%>
				<tr>
					<td colspan="9" height="1" class="LLine"></td></tr>
				<tr>
					<tr ><td colspan="9" align="center" class="RightText" >
<fmtUdiMissingAttributeDetail:message key="LBL_NOTHING_FOUND"/></td></tr>
				</tr> 
			<%}%>
			<tr>
				<td colspan="8" align="center" height="25" width="100%">
					<div class='exportlinks'>
						<fmtUdiMissingAttributeDetail:message key="DIV_EXPORT_OPT"/>:<img src='img/ico_file_excel.png' /> <a href="#"
							onclick="javascript:fnExportExcel();"><fmtUdiMissingAttributeDetail:message key="DIV_EXCEL"/>  </a>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2" height="35">
				<fmtUdiMissingAttributeDetail:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="${varClose}" gmClass="Button" onClick="fnClose();" tabindex="8" buttonType="Load" />&nbsp;
				</td>
			</tr>
		</table>

	</html:form>

		<%@ include file="/common/GmFooter.inc"%>
	</BODY>
</HTML>
