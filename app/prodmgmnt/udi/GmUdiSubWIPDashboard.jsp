
<%
	/**********************************************************************************
	 * File		 		: GmUdiSubWIPDashboard.jsp
	 * Desc		 		: 
	 * Version	 		: 1.0
	 * author			: Manikandan
	 ************************************************************************************/
%>
<!-- prodmgmnt\udi\GmUdiSubWIPDashboard.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtUdiSubWIPDashboard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtUdiSubWIPDashboard:setLocale value="<%=strLocale%>"/>
<fmtUdiSubWIPDashboard:setBundle basename="properties.labels.prodmgmnt.udi.GmUdiSubWIPDashboard"/>
<%@ page	import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ page 	import="java.util.Date"%>

<bean:define id="partNum" name="frmUdiSubDashboard" property="partNum"	type="java.lang.String"></bean:define>
<bean:define id="alProjectId" name="frmUdiSubDashboard"	property="alProjectId" type="java.util.ArrayList"></bean:define>
<bean:define id="gridXmlData" name="frmUdiSubDashboard"	property="gridXmlData" type="java.lang.String"></bean:define>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("UDI_WIP_DashBoard");
	String strSearch = GmCommonClass.parseNull((String) request.getAttribute("hSearch"));

	String strProdMgmntUdiJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_UDI");
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: UDI WIP DashBoard</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strProdMgmntUdiJsPath%>/GmUdiSubWIPDashboard.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script>
var objGridData;
objGridData = '<%=gridXmlData%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
	<html:form action="/gmUdiSubDashboard.do?method=udiWIPDashboard">
		<html:hidden property="strOpt" value="" name="frmUdiSubDashboard" />
		<html:hidden property="missingPartNum" name="frmUdiSubDashboard" />
		<input type="hidden" name="hSearch" value="<%=strSearch%>">

		<input type="hidden" name="diNumber" value="">
		<input type="hidden" name="partNum" value="">
		<table border="0" width="100%" class="DtTable1100" cellspacing="0"
			cellpadding="0">

			<tr>
				<td height="25" class="RightDashBoardHeader" colspan="7">&nbsp;<fmtUdiSubWIPDashboard:message key="LBL_UDI_WIP_DASHBOARD"/></td>
				<td height="25" class="RightDashBoardHeader"><fmtUdiSubWIPDashboard:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="RightTableCaption" height="30" colspan="1" align="right">
					<fmtUdiSubWIPDashboard:message key="LBL_GTIN_DI" var="varGTINDI"/><gmjsp:label type="RegularText" SFLblControlName="${varGTINDI}:"
						td="false" />
				</td>
				<td><textarea name="diNumberText" class="HtmlTextArea" rows="10" cols="30" onfocus="changeBgColor(this,'#AACCE8');"
					styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"><bean:write name="frmUdiSubDashboard" property="diNumberText" /></textarea></td>
				<td class="RightTableCaption" height="30" colspan="1" align="right"><select
					name="Cbo_Search" class="RightText" tabindex="2"
					onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');">
						<option value="0"><fmtUdiSubWIPDashboard:message key="LBL_CHOOSE_ONE"/>
						<option value="LIT"><fmtUdiSubWIPDashboard:message key="LBL_LITERAL"/>
						<option value="LIKEPRE"><fmtUdiSubWIPDashboard:message key="LBL_LIKE_PREFIX"/>
						<option value="LIKESUF"><fmtUdiSubWIPDashboard:message key="LBL_LIKE_SEFFIX"/>
				</select></td>
				<td class="RightTableCaption" height="30" colspan="1" align="right">
					<fmtUdiSubWIPDashboard:message key="LBL_PART" var="varPart"/><gmjsp:label type="RegularText" SFLblControlName="${varPart}:"
						td="false" />
				</td>
				<td><textarea name="partNumberText" class="HtmlTextArea" rows="10" cols="30" onfocus="changeBgColor(this,'#AACCE8');" 
					styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"><bean:write name="frmUdiSubDashboard" property="partNumberText" /></textarea></td>

			</tr>
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr class="Shade">

				<td class="RightTableCaption" height="25" colspan="1" align="right">
				<fmtUdiSubWIPDashboard:message key="LBL_PROJECT_ID" var="varProjectID"/><gmjsp:label type="RegularText" SFLblControlName="${varProjectID}:"
						td="false" />
				</td>
				<td class="RightTableCaption" height="25" colspan="2">&nbsp;<gmjsp:dropdown
						controlName="projectId" SFFormName="frmUdiSubDashboard"
						SFSeletedValue="projectId" SFValue="alProjectId" codeId="ID"
						codeName="IDNAME" defaultValue="[Choose One]" tabIndex="4"
						width="300" /></td>
				<td height="25" colspan="5"><fmtUdiSubWIPDashboard:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load"
						value="${varLoad}" style="width: 5em; height: 23px;" gmClass="Button"
						onClick="javascript:fnLoad();" tabindex="5" buttonType="Load" /></td>

			</tr>

			<%
				if (gridXmlData.indexOf("cell") != -1) {
			%>

			<tr>
				<td colspan="8">
					<div id="udiWipDashboard" style="height: 500px; width: 1100px"></div>
					<div id="pagingArea" style="width: 1100px"></div>
				</td>
			</tr>
			</tr>

			<tr>
				<td colspan="8" align="center" colspan="2" height="35"><fmtUdiSubWIPDashboard:message key="BTN_MISSING_ATTRIBUTE_REPORT" var="varMissingAttributeReport"/><gmjsp:button
						value="${varMissingAttributeReport}" gmClass="Button"
						onClick="fnMissingAttribute();"  buttonType="Load" />&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="8" align="center" height="25" width="100%">
					<div class='exportlinks'>
					<fmtUdiSubWIPDashboard:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' /> <a href="#"
							onclick="javascript:fnExportExcel();"> <fmtUdiSubWIPDashboard:message key="DIV_EXCEL"/> </a>
				</td>
			</tr>
			<%
				} else if (!gridXmlData.equals("")) {
			%>
			<tr>
				<td colspan="9" height="1" class="LLine"></td>
			</tr>
			<tr>
			<tr>
				<td colspan="9" align="center" class="RightText"><fmtUdiSubWIPDashboard:message key="LBL_NO_DATA"/>
					</td>
			</tr>
			</tr>
			<%}%>
	
		</table>

	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
