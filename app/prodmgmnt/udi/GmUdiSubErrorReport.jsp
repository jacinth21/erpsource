
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
	/**********************************************************************************
	 * File		 		: GmUdiSubErrorReport.jsp
	 * Desc		 		: This screen is used show UDI Error Report
	 * Version	 		: 1.0
	 * author			: rdinesh
	 ************************************************************************************/
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtUdiSubErrorReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\udi\GmUdiSubErrorReport.jsp -->
<fmtUdiSubErrorReport:setLocale value="<%=strLocale%>"/>
<fmtUdiSubErrorReport:setBundle basename="properties.labels.prodmgmnt.udi.GmUdiSubErrorReport"/>

<bean:define id="gridData" name="frmUdiSubReportDashboard" property="gridXmlData" type="java.lang.String"></bean:define>
<bean:define id="screenName" name="frmUdiSubReportDashboard" property="screenName" type="java.lang.String"></bean:define>
<bean:define id="udiErrorAcessfl" name="frmUdiSubReportDashboard" property="udiErrorAcessfl" type="java.lang.String"></bean:define>

<%

	String strProdMgmntUdiJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_UDI");

    String strBtnDisable = "true";	
    String strWikiTitle = GmCommonClass.getWikiTitle("UDI_ERROR_RPT");
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strSearch = GmCommonClass.parseNull((String) request.getAttribute("hSearch"));
	String strScreenType = GmCommonClass.parseNull((String) request.getParameter("screenName"));
	String strErrorXmlData = gridData;
	String strDivHeight = "400px";
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.prodmgmnt.udi.GmUdiSubErrorReport", strSessCompanyLocale);
	String strName = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_UDI_ERROR_REPORTS"));
	if(strScreenType.equals("DIATTR")){
		strErrorXmlData = GmCommonClass.parseNull((String) request.getParameter("gridErrorXmlData"));
		strDivHeight = "200px";
		strName = "UDI Error Details";
	}
	
	if(udiErrorAcessfl.equals("Y")){
	  strBtnDisable = "false";
	}
	
%>

<html>
<head>
<title>Globus Medical: UDI Error Reports</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
div.gridbox_dhx_skyblue table.obj tr td{
	word-wrap: break-word;
}
</style>

<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strProdMgmntUdiJsPath%>/GmUdiSubErrorReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
var objGridData = '<%=strErrorXmlData%>';
var format = '<%=strApplDateFmt%>';

</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad()">
	<html:form action="/gmUdiReport.do?method=udiErrorReport">
		<html:hidden property="strOpt" value="" />
		<html:hidden property="inputStr" value=""/> 
		<html:hidden property="screenName"/>
		<input type="hidden" name="hSearch" value="<%=strSearch%>">
		
		<table class="DtTable1100" border="0" cellpadding="0" cellspacing="0">

			<tr>
				<td class="RightDashBoardHeader" height="25" colspan="7"><%=strName %></td>
				<td height="25" align="right" class=RightDashBoardHeader><fmtUdiSubErrorReport:message key="IMG_ALT_HELP" var = "varHelp"/><img
					id='imgEdit' align="right" style='cursor: hand'
					src='<%=strImagePath%>/help.gif' title='${varHelp}'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<% if(strScreenType.equals ("")){ %>
			<tr>
				<td class="RightTableCaption" height="30" colspan="2" align="right">
					<fmtUdiSubErrorReport:message key="LBL_GTIN_DI" var="varGTIN"/><gmjsp:label type="RegularText" SFLblControlName="${varGTIN}:" 
					td="false" />
				</td>
				<td class="RightTableCaption" height="30" colspan="2">&nbsp;<html:text
						property="diNumber" styleClass="InputArea"
						name="frmUdiSubReportDashboard"
						onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="1" size="30" /></td>
				<td class="RightTableCaption" height="30" colspan="2" align="right"><select
					name="Cbo_Search" class="RightText" tabindex="2"
					onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');">
						<option value="0"><fmtUdiSubErrorReport:message key="LBL_CHOOSE_ONE"/>
						<option value="LIT"><fmtUdiSubErrorReport:message key="LBL_LITTERAL"/>
						<option value="LIKEPRE"><fmtUdiSubErrorReport:message key="LBL_LIKE_PREFIX"/>
						<option value="LIKESUF"><fmtUdiSubErrorReport:message key="LBL_LIKE_SUFFIX"/>
				</select>&nbsp;&nbsp;&nbsp;
				<fmtUdiSubErrorReport:message key="LBL_PART" var="varPart"/><gmjsp:label type="RegularText" SFLblControlName="${varPart}:"
						td="false" /></td>
				<td class="RightTableCaption" height="30" colspan="2" align="left">&nbsp;<html:text
						property="partNum" styleClass="InputArea"
						name="frmUdiSubReportDashboard"
						onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="3" size="30" /></td>
			</tr>
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<td class="RightTableCaption" height="30" colspan="2" align="right">
					<fmtUdiSubErrorReport:message key="LBL_ERROR_FORM_DATE" var="varErrorFromDate"/><gmjsp:label type="RegularText" SFLblControlName="${varErrorFromDate}:"
						td="false" />
				</td>
				<td class="RightTableCaption" height="30" colspan="2">&nbsp;<gmjsp:calendar
						SFFormName="frmUdiSubReportDashboard" controlName="errorFromDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="4" /></td>
				<td class="RightTableCaption" height="30" colspan="2" align="right">
					<fmtUdiSubErrorReport:message key="LBL_TO_DATE" var="varToDate"/><gmjsp:label type="RegularText" SFLblControlName="${varToDate}:"
						td="false" />
				</td>
				<td class="RightTableCaption" height="30" colspan="2" align="left">&nbsp;<gmjsp:calendar
						SFFormName="frmUdiSubReportDashboard" controlName="errorToDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="5" /></td>
			</tr>
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" height="30" colspan="2" align="right">
					<fmtUdiSubErrorReport:message key="LBL_ERROR_STATUS" var="varErrorStatus"/><gmjsp:label type="RegularText" SFLblControlName="${varErrorStatus}:"
						td="false" />
				</td>
				<td class="RightTableCaption" height="30" colspan="2">&nbsp;<gmjsp:dropdown
						controlName="errorStatus" SFFormName="frmUdiSubReportDashboard"
						SFSeletedValue="errorStatus" SFValue="alErrorStatus" codeId="CODEID"
						codeName="CODENM" defaultValue="[Choose One]" tabIndex="6" /></td>
				<td class="RightTableCaption" height="30" colspan="2" align="right">
					<fmtUdiSubErrorReport:message key="LBL_ERROR_CATEGORY" var="varErrorCategory"/><gmjsp:label type="RegularText" SFLblControlName="${varErrorCategory}:"
						td="false" />
				</td>
				<td class="RightTableCaption" height="30" colspan="2" align="left">&nbsp;<gmjsp:dropdown
						controlName="errorCategory" SFFormName="frmUdiSubReportDashboard"
						SFSeletedValue="errorCategory" SFValue="alErrorCategory"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
						tabIndex="7" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtUdiSubErrorReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load"
						value="${varLoad}" style="width: 5em; height: 23px;" gmClass="Button"
						onClick="javascript:fnLoad();" tabindex="8" buttonType="Load" /></td>
			</tr>
			
			<%
			}if (strErrorXmlData.indexOf("cell") != -1) {
			%>
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr>
				<td colspan="8"><div id="dataGridDiv" style="width: 1100px; height: <%=strDivHeight%>"></div>
					<div id="pagingArea" style="width: 1100px"></div></td>
			</tr>
	<% if(strScreenType.equals ("")){ %>
			<tr>
				<td align="center" colspan="8"><br>
					<div class='exportlinks'>
						<fmtUdiSubErrorReport:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp; <a
							href="#" onclick="fnExport();"> <fmtUdiSubErrorReport:message key="DIV_EXCEL"/></a>
					</div></td>
			</tr>
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr>
				<td colspan="8" align="center" height="30"><fmtUdiSubErrorReport:message key="BTN_CLOSE_ERROR" var="varCloseError"/><gmjsp:button
						name="Btn_Close_Error" value="${varCloseError}"
						style="width: 8em; height: 23px;" gmClass="Button" disabled ="<%=strBtnDisable %>"
						onClick="javascript:fnCloseError();"
						buttonType="Save" /></td>
			</tr>
			
				<%
	}} else if (!strErrorXmlData.equals("")) {
				%>
			
			<tr>
				<td colspan="8" height="1" class="LLine"></td>
			</tr>
			<tr>
				<td colspan="8" align="center" class="RightText"><fmtUdiSubErrorReport:message key="LBL_NO_DATA"/>

					</td>
			</tr>
			<%} %>
			
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>