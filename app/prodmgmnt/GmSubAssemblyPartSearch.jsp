 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmSubAssemblyPartSearch.jsp
 * Desc		 		: This screen is used for searching parts and sub assembly parts
 * Version	 		: 1.0
 * author			: Joe Prasanna Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ taglib prefix="fmtSubAssemblyPartSearch" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmSubAssemblyPartSearch.jsp -->
<fmtSubAssemblyPartSearch:setLocale value="<%=strLocale%>"/>
<fmtSubAssemblyPartSearch:setBundle basename="properties.labels.prodmgmnt.GmSubAssemblyPartSearch"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strPartNum = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	String strChecked = GmCommonClass.parseNull((String)request.getAttribute("hStatus"));
	String strCheckedSubAsmId = "";
	String strCheckedPnum = "";
	log.debug(" StrChecked uis " + strChecked);
	String strPartNumId = ""; 
	String strPartDesc = "";
	String strSubAsm = "";
	String strSubAsmDesc = "";
	String strRev = "";
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.prodmgmnt.GmSubAssemblyPartSearch", strSessCompanyLocale);
	
	if(strChecked.equals("rdPartNumber"))
	{
		 strCheckedPnum = "checked";
		 strPartNumId = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PART"));
		 strPartDesc = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PART_DESCRIPTION"));
		 strSubAsm = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUB_ASM_ID"));
		 strSubAsmDesc = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUB_ASSEMBLY_DESCRIPTION"));
		 strRev = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUB_ASM_REV"));
	 }
	else
	{
		strCheckedSubAsmId = "checked";
		 strPartNumId = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUB_ASM_ID"));
		 strPartDesc = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUB_ASSEMBLY_DESCRIPTION"));
		 strSubAsm = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PARENT_PART"));
		 strSubAsmDesc = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PARENT_PART_DESCRIPTION"));
		strRev = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PART_REV"));
	}
		
	ArrayList alSearchReport = new ArrayList();
	int intCount = 0;
	
	if (hmReturn != null) 
	{
		alSearchReport = (ArrayList)hmReturn.get("SEARCHREPORT");
		intCount = alSearchReport.size();
	}

	HashMap hmLoop = new HashMap();

	String strId = "";
	String strProjName = "";

	HashMap hmTempLoop = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number / Sub Assembly Part  Search</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnGo()
{
	document.frmProdMgmt.hAction.value = "Go";
	fnStartProgress('Y');
	document.frmProdMgmt.submit();
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmProdMgmt" method="POST" action="<%=strServletPath%>/GmPartNumServlet">
<input type="hidden" name="hAction" value="">

	<table border="0" cellspacing="0" cellpadding="0"  CLASS ="dttable900">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtSubAssemblyPartSearch:message key="LBL_PART_NUMBER_SUB_ASSEMBLY"/></td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="250" align="right" class="RightText" HEIGHT="24" width="150"><fmtSubAssemblyPartSearch:message key="LBL_ENTER_PARTS_SEPERATED"/>:</td>
						<td  class="RightText">&nbsp;<input type="text" size="40" value="<%=strPartNum%>" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						</td>
						<td>
						<input type ="radio" name="PartNum" value="rdPartNumber" <%=strCheckedPnum%> > <fmtSubAssemblyPartSearch:message key="LBL_PART_NUMBER"/><BR>
						<input type ="radio" name="PartNum" value="rdSubAssemblyId"  <%=strCheckedSubAsmId%> > <fmtSubAssemblyPartSearch:message key="LBL_SUB_ASSEMBLY_ID"/>
						</td>
						<fmtSubAssemblyPartSearch:message key="BTN_GO" var="varGo"/>
						<td width="200" >&nbsp;<gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_Submit" gmClass="button" buttonType="Load" onClick="javascript:fnGo()" tabindex="3" /></td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
				</table>
			</td>
		</tr>
<% if (strAction.equals("Reload"))  { %>		
		<tr><td class="Line" height="1"></td></tr>
		<tr><td height="25" class="RightDashBoardHeader">&nbsp;<fmtSubAssemblyPartSearch:message key="LBL_SEARCH_RESULT"/></td></tr>
		<tr>
			<td>
				<display:table name="subassemblydetails" class="its" id="currentRowObject" decorator="com.globus.displaytag.beans.DTPDSubAsmSearchWrapper" export ="true"> 
				<display:column property="PARTNUM" title="<%=strPartNumId%>" group="1" sortable="true" class="alignleft"/>
				<display:column property="PARTDESC" escapeXml="true" title="<%=strPartDesc%>" group="2" style="width:260px"/> <!-- Using ID alias Name to display the View Icon for corrresponding Action-->
				<fmtSubAssemblyPartSearch:message key="LBL_PROJ_NAME" var="varProjectName"/><display:column property="PROJECTNAME" title="${varProjectName}" group="3" sortable="true" style="width:180px" />
				<display:column property="SUBASSEMID" title="<%=strSubAsm%>"  sortable="true" style="width:60px" />
				<display:column property="SUBASSEMBDESC" title="<%=strSubAsmDesc%>" sortable="true" style="width:300px"  />
				<display:column property="SUBASSEMREV" title="<%=strRev%>"  style="width:60px" />
				</display:table> 
			</td>
		</tr>
<% } %>
	<tr><td class="Line" height="1"></td></tr>	
	</table>


</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
