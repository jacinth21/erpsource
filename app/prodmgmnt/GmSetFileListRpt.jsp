 
<%
/**********************************************************************************
 * File		 		: GmSetFileListRpt.jsp
 * Desc		 		: This screen is used for the no.of files uploaded for set 
 * Version	 		: 1.0
 * author			: Dhana Reddy
************************************************************************************/
%>
<!-- prodmgmnt\GmSetFileListRpt.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtSetFileListRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSetFileListRpt:setLocale value="<%=strLocale%>"/>
<fmtSetFileListRpt:setBundle basename="properties.labels.prodmgmnt.GmSetFileListRpt"/>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>

<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle ="SET_FILE_REPORT";
	String strSetId ="";
	String strGridData ="";
	String strScreenType = "";
	ArrayList alSetList = new ArrayList();
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	if (hmReturn != null)
	{
		alSetList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETMASTERLIST"));
	}
	strSetId = GmCommonClass.parseNull((String)request.getAttribute("SETID"));
	strScreenType = GmCommonClass.parseNull((String)request.getAttribute("SCREENTYPE"));
	strGridData = GmCommonClass.parseNull((String)request.getAttribute("GRIDATA"));
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Set File List Report</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmSetFileListRpt.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script>
var objGridData;
objGridData = '<%=strGridData%>';
var gridObj ='';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();" >
	<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmSetMasterServlet">
    <input type="hidden" name="hAction" value="<%=strhAction%>">
    <input type="hidden" name="hOpt" value="SET">
    <input type="hidden" name="hReport" value="">
		<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="4" height="25" class="RightDashBoardHeader">&nbsp;<fmtSetFileListRpt:message key="LBL_INSPECTION"/></td>
				<td class="RightDashBoardHeader"><fmtSetFileListRpt:message key="IMG_ALT_HELP" var="varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle(strWikiTitle)%>');" /></td>
			</tr>
			<%if(!strScreenType.equals("Hyperlink")){ %>
			<tr>
				<td>
					<table>
						<tr>
							<td class="RightTableCaption" HEIGHT="30" align="right" width="10%"><fmtSetFileListRpt:message key="LBL_SET_NAME"/>
								:</td>
							<!-- Custom tag lib code modified for JBOSS migration changes -->
							<td colspan="3">&nbsp;<gmjsp:dropdown controlName="Cbo_SetId" seletedValue="<%=strSetId%>" value="<%=alSetList%>" 
								codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								<fmtSetFileListRpt:message key="BTN_LOAD" var="varLoad"/><gmjsp:button gmClass="button" name="Btn_Go" value="&nbsp;${varLoad}&nbsp;"
								buttonType="Load" onClick="fnLoad()" /></td>
						</tr>
					</table>
				</td>
			</tr>
			<%} %>
			<%if( strGridData.indexOf("cell") != -1){%>
				<tr><td colspan="5" align="center"><div  id="SetFileReport" style="" height="450px" width="800px"></div></td></tr>						
			<%}else if(!strGridData.equals("")){%>
				<tr><td height="1" colspan="5" class="LLine"></td></tr>
				<tr><td colspan="5" align="center" class="RightText"><fmtSetFileListRpt:message key="LBL_NOTHING_FOUND"/></td></tr>
			<%}else{%>
			<tr><td height="1" colspan="5" class="LLine"></td></tr>
				<tr><td colspan="5" align="center" class="RightText"><fmtSetFileListRpt:message key="LBL_NO_DATA"/></td></tr>
			<%} %>		
			<%if( strGridData.indexOf("cell") != -1) {%>
				<tr><td height="1" colspan="5" class="LLine"></td></tr>
				<tr>
				<td colspan="5" align="center">
			    	<div class='exportlinks'><fmtSetFileListRpt:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"> <fmtSetFileListRpt:message key="DIV_EXCEL"/> </a></div>
				</td>
				</tr>
			<%}%>	
			<%if(strScreenType.equals("Hyperlink")){ %>
			<tr>
				<td align="center"  colspan="5">
					<fmtSetFileListRpt:message key="BTN_CLOSE" var="varClose"/><gmjsp:button gmClass="button" name="Btn_Go" buttonType="Load" value="&nbsp;${varClose}&nbsp;" onClick="window.close();" />
				</td>
			</tr>
			<tr></tr>
			<%} %>
		</table>
	</FORM>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
