 <%
/**********************************************************************************
 * File		 		: GmSetMapSetup.jsp
 * Desc		 		: This screen is used for the Set and Part Number Mapping
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!-- prodmgmnt\GmSetMapSetup.jsp.prod.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtSetMapSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSetMapSetup:setLocale value="<%=strLocale%>"/>
<fmtSetMapSetup:setBundle basename="properties.labels.prodmgmnt.GmSetMapSetup"/>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strSessDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
 	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strhOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
	String strUserStatusFlag = GmCommonClass.parseNull((String)request.getAttribute("strUserStatusFlag"));
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strCheckedCrit = "";
	String strSetId =	"";
	String strPartNum = "";
	String strSetNm = "";
	String strInSetFl = "";
	String strCritFl = "";
	String strDesc = "";
	String strQty = "";
	String strSelectedIds = "";
	String strId = "";
	String strCriticalQty = "";
	String strCriticalTag = "";
	String strCriticalType = "";
	String strUnitType = "";
	String strDisabled = "disabled";
	String strSetTypeFL = ""; 
	String  strPartFamilyFL="", strProdAttributeValue="";
	ArrayList alSetList = new ArrayList();
	ArrayList alSetPartMapList = new ArrayList();
	ArrayList alCritType = new ArrayList();
	ArrayList alUnitType = new ArrayList();
	 
	if (hmReturn != null)
	{
		alSetList = (ArrayList)hmReturn.get("SETMASTERLIST");
		if (strhAction.equals("Go"))
		{
			alSetPartMapList = (ArrayList)hmReturn.get("SETMAPPEDLIST");
			strSetId = (String)request.getAttribute("hSetId");
			alCritType = (ArrayList)hmReturn.get("SETCRITTYPE");
			alUnitType = (ArrayList)hmReturn.get("SETUNITTYPE");
		}
	}

	int intMapSize = alSetPartMapList.size();
	int intSize = 0;
	HashMap hcboVal = null;
	if (strSessDeptId.equals("Z")|| strUserStatusFlag.equals("Y"))
	{
		strDisabled = "";
	}
	String checkCtriFl = ""; 
	String checkTaggableFl = ""; 
	
	for (int i=0;i<intMapSize;i++)
		{
			
			hcboVal = (HashMap)alSetPartMapList.get(i);
			 
			strCritFl = GmCommonClass.parseNull((String)hcboVal.get("CRITICALFL")); 
			
			// If Set type = "Sales/Consignment" and no critical Flag checked ,
			strSetTypeFL = GmCommonClass.parseNull((String)hcboVal.get("SETTYPEFL"));  //Sales or consignment 
		 
			 if(strSetTypeFL.equals("Y")&&strCritFl.equals("Y"))
			 {	
			  	checkCtriFl = "Y";
			 }
			// If Part number family = Graphical Case and Taggable =Y, 
			strPartFamilyFL = GmCommonClass.parseNull((String)hcboVal.get("PARTFAMILYFL")); // Graphical Case 
			strProdAttributeValue = GmCommonClass.parseNull((String)hcboVal.get("PRODATTRIBUTEVALUE")); // Taggable Part 4052
			
			 if( strPartFamilyFL.equals("Y") && strProdAttributeValue.equals("Y"))
			 {	
			  	checkTaggableFl = "Y";
			 }
			
		}
%>
<HTML>
<jsp:useBean id="brandBean" type="com.globus.common.beans.GmResourceBundleBean" class="com.globus.common.beans.GmResourceBundleBean" scope="request"> </jsp:useBean>
<TITLE><%=brandBean.getProperty("Title")%></TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmSetMapSetup.js"></script>

<script>
var flaghA = false;
var disabled = "<%=strDisabled%>";
var count = '<%=intMapSize%>';
var strUserStatusFlag ='<%=strUserStatusFlag%>';
function fnVoidSetPart(partNum, partDesc)
{
	document.frmVendor.hAction.value = "Load";
	document.frmVendor.hTxnId.value = partNum;
	document.frmVendor.hTxnName.value = partDesc;
	document.frmVendor.action ="<%=strServletPath%>/GmCommonCancelServlet";
	document.frmVendor.submit();
}

 
 
</SCRIPT>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnSetSelections();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmSetMapServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<INPUT type="hidden" name="inputString" value="">
<input type="hidden" name="hOpt" value="<%=strhOpt%>">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hTxnName" value="">
<input type="hidden" name="hCancelType" value="VSPMP">
	<table border="0" width="752" cellspacing="0" cellpadding="0">
		<tr><td class="Line" colspan="3" height="1"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"><img src="<%=strImagePath%>/spacer.gif" width="1"/></td>
			<td height="25" class="RightDashBoardHeader">&nbsp;<%=brandBean.getProperty("Title")%></td>
			<td bgcolor="#666666" width="1" rowspan="3"><img src="<%=strImagePath%>/spacer.gif" width="1"/></td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr>
			<td width ="750" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" colspan="2" HEIGHT="40"><%=brandBean.getProperty("CaptionName")%> :
						&nbsp;<gmjsp:dropdown ControlName="Cbo_Set"  SeletedValue="<%=strSetId%>" 	
							   Value="<%=alSetList%>" CodeId = "ID"  CodeName = "IDNAME" DefaultValue= "[Choose One]"  />
							&nbsp;&nbsp;<fmtSetMapSetup:message key="BTN_GO" var="varGo"/><gmjsp:button gmClass="button" name="Btn_Go" buttonType="Load" value="${varGo}&nbsp;&nbsp;" onClick ="fnGo()" />
						</td>
					</tr>
					
<%
					if (strhAction.equals("Go"))
					{
%>
					<tr><td class="Line" height="1"></td></tr>
					<tr>
						<td colspan="2" align="center">
						<div style="overflow:auto; height:350px;">
							<TABLE cellpadding="1" cellspacing="1" border="0" width="99%" bgcolor="#666666"  id="PriceTable">
							<tr><td bgcolor="#eeeeee" colspan="9" height="1"></td></tr>
								<thead>
									<tr class="Shade" style="position:relative; top:expression(this.offsetParent.scrollTop);">
										<th nowrap class="RightText" height="35" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_PART_NUMBER"/></th>
										<th nowrap class="RightText" width="350">&nbsp;<fmtSetMapSetup:message key="LBL_DESCRIPTION"/></th>
										<th nowrap class="RightText" align="center">&nbsp;<%=brandBean.getProperty("CaptionQty")%></th>
										<th nowrap class="RightText" align="center" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_FLAG"/><BR></th>
										<th nowrap class="RightText" align="center" width="60" id="strCrit_Flag" >&nbsp;<fmtSetMapSetup:message key="LBL_CRITICAL_FLAG"/><BR></th>
										<th nowrap class="RightText" align="center" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_CRITICAL_QTY"/><BR></th>
										<th nowrap class="RightText" align="center" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_CRITICAL_TYPE"/><BR></th>
										
										<%  if (strhOpt.equals("BOMMAP")) {%>
										<th nowrap class="RightText" align="center" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_TAG"/></th>
										<th nowrap class="RightText" align="center" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_UNIT_OF_MEASUREMENT"/><br></th>
										<%
									  		}else {
										%>	
										
										<th nowrap class="RightText" align="center" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_CRITICAL_TAG"/><BR></th>
										
										<%
									  		} 
										%>
									</TR>
								</thead>
								<tbody>
<%
					String strTemp = "";
					String strUnit = "";
					hcboVal = new HashMap();
					int count=0;
			  		for (int i=0;i<intMapSize;i++)
			  		{
			  			
			  			hcboVal = (HashMap)alSetPartMapList.get(i);
			  			strId = (String)hcboVal.get("MID");
			  			strPartNum = (String)hcboVal.get("PNUM");
						strDesc = GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"));
						strDesc = GmCommonClass.formatSpecialCharFromDB(strDesc);
						strQty = GmCommonClass.parseZero((String)hcboVal.get("QTY"));
						strInSetFl = GmCommonClass.parseNull((String)hcboVal.get("INSETFL"));
						strCritFl = GmCommonClass.parseNull((String)hcboVal.get("CRITICALFL"));
						strChecked = strInSetFl.equals("Y")?"checked":"";
						strCheckedCrit = strCritFl.equals("Y")?"checked":"";
						strCriticalQty = GmCommonClass.parseNull((String)hcboVal.get("CRITQTY"));
						strCriticalQty = strCriticalQty.equals("") && strCritFl.equals("Y")?strQty:strCriticalQty;
						strCriticalTag = GmCommonClass.parseNull((String)hcboVal.get("CRITTAG"));
						strCriticalType = GmCommonClass.parseNull((String)hcboVal.get("CRITTYPE"));
						strUnitType = GmCommonClass.parseNull((String)hcboVal.get("UNITTYPE"));
					 
						strProdAttributeValue = GmCommonClass.parseNull((String)hcboVal.get("PRODATTRIBUTEVALUE")); // Taggable Part 4052, Graphical Case
						
						
						strTemp = "Cbo_CritType"+i;
						strUnit = "Cbo_UnitType"+i;
%>
									<tr bgcolor="#ffffff" onMouseOver="changeTRBgColor(<%=i%>,'#EEEEEE');" id="tr<%=i%>">
										<td class="RightText" HEIGHT="20">&nbsp; <a href="javascript:fnVoidSetPart('<%=strId%>', '<%=strDesc%>');" title="Click to Void Set Part"> 
										<img src='<%= strImagePath%>/void.jpg' border='0' height='14' width='14' /></a>&nbsp;<%=strPartNum%><input type="hidden" name="Txt_PNum<%=i%>" value="<%=strPartNum%>">
										<input type="hidden" name="hId<%=i%>" value="<%=strId%>"></td>
										<td class="RightText">&nbsp;<%=strDesc%></td>
										<td class="RightText" align="center"><input type="text" size="2" class="InputArea" value="<%=strQty%>" name="Txt_Qty<%=i%>"></td>
										<td class="RightText" align="center"><input type="checkbox" <%=strChecked%> name="Chk_Set<%=i%>"></td>
										<td class="RightText" align="center"><input type="checkbox" <%=strCheckedCrit%> <%=strDisabled%> name="Chk_Crit<%=i%>" id="Chk_Crit<%=i%>"  ></td>
										<td class="RightText" align="center"><input type="text" size="2" class="InputArea" <%=strDisabled%> value="<%=strCriticalQty%>" name="Txt_CritQty<%=i%>"></td>
										<td class="RightText" align="center"><gmjsp:dropdown ControlName= "<%=strTemp%>" Disabled="<%=strDisabled%>" SeletedValue="<%=strCriticalType%>" 	
							   			Value="<%=alCritType%>" CodeId = "CODEID"  CodeName = "CODENM" DefaultValue= "[Choose One]"  /></td>
										<td class="RightText" align="center"><input type="text" size="2" class="InputArea" <%=strDisabled%> value="<%=strCriticalTag%>" name="Txt_CritTag<%=i%>"></td>
										
										 
										<input type="hidden" name="strProdAttributeValue<%=i%>" id="strProdAttributeValue<%=i%>" value="<%=strProdAttributeValue%>" />
										
										
									<%  if (strhOpt.equals("BOMMAP")) {%>
					
										<td class="RightText" align="center"><gmjsp:dropdown ControlName= "<%=strUnit%>" Disabled="<%=strDisabled%>" SeletedValue="<%=strUnitType%>" 	
							   												Value="<%=alUnitType%>" CodeId = "CODEID"  CodeName = "CODENM"   /></td>
									<%
												}
									%>								
									</tr>
<%
			  		}
%>								
								</tbody>
							</table>
						</div>
						</td>
				   </tr>
				   <tr><td>
				   <%  		if (!strhOpt.equals("BOMMAP")) 
				   				{ if (!checkCtriFl.equals("Y")) { 
								 %>
								<div id="strCritical_Qty"><strong><font color="red"><center>No Part is marked as critical in the set</center></font></strong></div>
								<%
									}	if (!checkTaggableFl.equals("Y")) { 
								%>
								<div id="strTaggable_part"><strong><font color="red"><center>No Taggable Part in this set</center></font></strong></div>
							<%
									}
								}
							%>
					</td></tr>
				   <tr>
						<td colspan="2" align="center" height="35">				   
						   	<jsp:include page="/common/GmUpdationHistory.jsp" >
							</jsp:include>
						</td>
					</tr>
				   <tr>
						<td colspan="2" align="center" height="35">
							<INPUT type="hidden" name="hCnt" value="<%=intMapSize%>">
							<input type="hidden" name="hSelectedIds" value="<%=strSelectedIds%>">
							<fmtSetMapSetup:message key="BTN_ADD_ROW" var="varAddRow"/><gmjsp:button value="${varAddRow}" gmClass="button" buttonType="Save" onClick="fnAddRow('PriceTable', '<%=strhOpt%>');" tabindex="16" />&nbsp;&nbsp;
							<fmtSetMapSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}&nbsp;&nbsp;" gmClass="button" buttonType="Save" onClick="fnSubmit('<%=strhOpt%>');" tabindex="16" />&nbsp;
						</td>
					</tr>
<%
					}
%>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
<div style="visibility:hidden" id="cbohtml">
<gmjsp:dropdown ControlName="Cbo_CritType" Value="<%=alCritType%>" Disabled="<%=strDisabled%>" CodeId="CODEID" CodeName="CODENM" DefaultValue="[Choose One]"/>
</div>

<%  if (strhOpt.equals("BOMMAP")) {%>
<div style="visibility:hidden" id="cbouhtml">
<gmjsp:dropdown ControlName= "Cbo_UnitType" Disabled="<%=strDisabled%>"   Value="<%=alUnitType%>" CodeId = "CODEID"  CodeName = "CODENM"   />
</div>
<%
	}
%>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
