
<%
	/**********************************************************************************
	 * File		 		: GmPdUDISetup.jsp
	 * Desc		 		: This screen is used for the 
	 * Version	 		: 1.0
	 * author			: 
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtPdUDISetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmPdUDISetup.jsp -->
<fmtPdUDISetup:setLocale value="<%=strLocale%>"/>
<fmtPdUDISetup:setBundle basename="properties.labels.prodmgmnt.GmPdUDISetup"/>
<bean:define id="gridData" name="frmUDIProductDevelopment" property="gridData" type="java.lang.String"/>
<bean:define id="alSterilMthod" name="frmUDIProductDevelopment" property="alSterilMthod" type="java.util.ArrayList"/>
<bean:define id="alSizeType" name="frmUDIProductDevelopment" property="alSizeType" type="java.util.ArrayList"/>
<bean:define id="alSizeUOM" name="frmUDIProductDevelopment" property="alSizeUOM" type="java.util.ArrayList"/>
<bean:define id="hmDropdown" name="frmUDIProductDevelopment" property="hmAttrVal" type="java.util.HashMap"/>
<bean:define id="strMsgFl" name="frmUDIProductDevelopment" property="msgFl" type="java.lang.String"> </bean:define>
<bean:define id="hmResult" name="frmUDIProductDevelopment" property="hmResult" type="java.util.HashMap"/>

<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response, session);
	HashMap hmsteril = new HashMap();
	HashMap hmsizetype = new HashMap();
	HashMap hmuom = new HashMap();

	ArrayList alLength = new ArrayList();
	ArrayList alArea = new ArrayList();
	ArrayList alWeight = new ArrayList();
	ArrayList alTotalVolume = new ArrayList();
	ArrayList alGauge = new ArrayList();
	ArrayList alAngle = new ArrayList();
	ArrayList alPressure = new ArrayList();
	ArrayList alProjectList = new ArrayList();
	ArrayList alYesNo = new ArrayList();
	
	HashMap hProjList = null;
	HashMap hYesNo = null;
	HashMap hSterilMthod = null;
	HashMap hSizeType = null;
	HashMap hSizeUOM = null;
	
	alLength = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alLength"));
	alArea = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alArea"));
	alWeight = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alWeight"));
	alTotalVolume = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alTotalVolume"));
	alGauge = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alGauge"));
	alAngle = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alAngle"));
	alPressure = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alPressure"));
	alProjectList = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alProjList"));
	alYesNo = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alYesNo"));
	//alSterilMthod = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alSterilMthod"));
	//alSizeType = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alSizeType"));
	//alSizeUOM = GmCommonClass.parseNullArrayList((ArrayList) hmDropdown.get("alSizeUOM"));
	
	String strWikiTitle = GmCommonClass.getWikiTitle("PD_UDI_SETUP");
	String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
	String strMaxData = GmCommonClass.parseNull((String)hmResult.get("MAX_DATA"));
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Part Number Search</TITLE>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script type="text/javascript" src="<%=strProdMgmntJsPath%>/GmPdUDISetup.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>

 <script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script> 
<%-- <script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxMenu/dhtmlxmenu.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxMessage/dhtmlxmessage.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script> --%>

<style type="text/css">
        .dhtmlx-myCss{
        	FONT-SIZE: 18pt;
            font-weight:bold !important;
            FONT-FAMILY: verdana, arial, sans-serif;
            color:white !important;
            background-color:green !important;
            text-align: center;
        }
        .dhtmlx-myErrorCss{
        	FONT-SIZE: 18pt;
            font-weight:bold !important;
            FONT-FAMILY: verdana, arial, sans-serif;
            color:white !important;
            background-color:red !important;
            text-align: center;
        }
        
        .even{
			background-color:#ffffff !important;
		}
		.uneven{
			background-color:#dcedfc !important;;
		}
		
		div.gridbox table.hdr td {
   
    color: #000000 !important;
    background-color: #d2e9fc !important;
    }
    div.gridbox_material.gridbox .xhdr {
    background:#dcedfc !important;
}
    </style>
<script>
var objGridData;
<%-- objGridData ='<%=gridData%>'; --%>
var msgFl = '<%=strMsgFl%>';
var validate_fl = false;
var arraysteril = new Array();
var arraysizetype = new Array();
var arraysizeUOM = new Array();
var arrayLength = new Array();
var arrayArea = new Array();
var arrayWeight = new Array();
var arrayTotalVol = new Array();
var arrayGauge = new Array();
var arrayAngle = new Array();
var arrayPressure = new Array();
var maxPdData = '<%=strMaxData%>';
var projectLen = <%=alProjectList.size()%>;
var containsLatexLen = <%=alYesNo.size()%>;
var sterilMethodLen = <%=alSterilMthod.size()%>;
var sizeTypeLen = <%=alSizeType.size()%>;
var sizeUOMLen = <%=alSizeUOM.size()%>;

//For setting the drop down value of Project List
<%	
hProjList = new HashMap();	
for (int i=0;i< alProjectList.size();i++)
{
	hProjList = (HashMap) alProjectList.get(i); 
%>
var projectArr<%=i%> ="<%=GmCommonClass.parseNull((String)hProjList.get("ID"))%>,<%=GmCommonClass.parseNull((String)hProjList.get("ID"))%>";
<%
}
%>
//For setting the drop down value of yes/No
<%	
hYesNo = new HashMap();	
for (int i=0;i< alYesNo.size();i++)
{
	hYesNo = (HashMap) alYesNo.get(i); 
%>
var containsLatex<%=i%> ="<%=GmCommonClass.parseNull((String)hYesNo.get("CODEID"))%>,<%=GmCommonClass.parseNull((String)hYesNo.get("CODENM"))%>";
<%
}
%>
//For setting the drop down value of steril method
<%	
hSterilMthod = new HashMap();	
for (int i=0;i< alSterilMthod.size();i++)
{
	hSterilMthod = (HashMap) alSterilMthod.get(i); 
%>
var sterilMethod<%=i%> ="<%=GmCommonClass.parseNull((String)hSterilMthod.get("CODEID"))%>,<%=GmCommonClass.parseNull((String)hSterilMthod.get("CODENM"))%>";
<%
}
%>
//For setting the drop down value of size method
<%	
hSizeType = new HashMap();	
for (int i=0;i< alSizeType.size();i++)
{
	hSizeType = (HashMap) alSizeType.get(i); 
%>
var sizeType<%=i%> ="<%=GmCommonClass.parseNull((String)hSizeType.get("CODEID"))%>,<%=GmCommonClass.parseNull((String)hSizeType.get("CODENM"))%>";
<%
}
%>

//For setting the drop down value of UOM
<%	
hSizeUOM = new HashMap();	
for (int i=0;i< alSizeUOM.size();i++)
{
	hSizeUOM = (HashMap) alSizeUOM.get(i); 
%>
var sizeUOM<%=i%> ="<%=GmCommonClass.parseNull((String)hSizeUOM.get("CODEID"))%>,<%=GmCommonClass.parseNull((String)hSizeUOM.get("CODENM"))%>";
<%
}
%>


//dropdown values for sterilization method
<%for (int i = 0; i < alSterilMthod.size(); i++) {
				hmsteril = (HashMap) alSterilMthod.get(i);%>
	arraysteril.push('<%=hmsteril.get("CODEID")%>'+","+'<%=hmsteril.get("CODENM")%>');
<%}%>

//dropdown values for size type value
<%for (int i = 0; i < alSizeType.size(); i++) {
				hmsizetype = (HashMap) alSizeType.get(i);%>
	arraysizetype.push('<%=hmsizetype.get("CODEID")%>'+","+'<%=hmsizetype.get("CODENM")%>');
<%}%>

//dropdown values for UOM value
<%for (int i = 0; i < alSizeUOM.size(); i++) {
				hmuom = (HashMap) alSizeUOM.get(i);%>
	arraysizeUOM.push('<%=hmuom.get("CODEID")%>'+","+'<%=hmuom.get("CODENM")%>');
<%}%>

//dropdown values for Length
<%for (int i = 0; i < alLength.size(); i++) {
				hmsteril = (HashMap) alLength.get(i);%>
arrayLength.push('<%=hmsteril.get("CODEID")%>'+","+'<%=hmsteril.get("CODENM")%>');
<%}%>
//dropdown values for Area
<%for (int i = 0; i < alArea.size(); i++) {
				hmsteril = (HashMap) alArea.get(i);%>
arrayArea.push('<%=hmsteril.get("CODEID")%>'+","+'<%=hmsteril.get("CODENM")%>');
<%}%>
//dropdown values for Weight
<%for (int i = 0; i < alWeight.size(); i++) {
				hmsteril = (HashMap) alWeight.get(i);%>
arrayWeight.push('<%=hmsteril.get("CODEID")%>'+","+'<%=hmsteril.get("CODENM")%>');
<%}%>
//dropdown values for TotalVolume
<%for (int i = 0; i < alTotalVolume.size(); i++) {
				hmsteril = (HashMap) alTotalVolume.get(i);%>
arrayTotalVol.push('<%=hmsteril.get("CODEID")%>'+","+'<%=hmsteril.get("CODENM")%>');
<%}%>
//dropdown values for Gauge
<%for (int i = 0; i < alGauge.size(); i++) {
				hmsteril = (HashMap) alGauge.get(i);%>
arrayGauge.push('<%=hmsteril.get("CODEID")%>'+","+'<%=hmsteril.get("CODENM")%>');
<%}%>
//dropdown values for Angle
<%for (int i = 0; i < alAngle.size(); i++) {
				hmsteril = (HashMap) alAngle.get(i);%>
arrayAngle.push('<%=hmsteril.get("CODEID")%>'+","+'<%=hmsteril.get("CODENM")%>');
<%}%>

//dropdown values for Pressure
<%for (int i = 0; i < alPressure.size(); i++) {
				hmsteril = (HashMap) alPressure.get(i);%>
arrayPressure.push('<%=hmsteril.get("CODEID")%>'+","+'<%=hmsteril.get("CODENM")%>');
<%}%>
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"
	onload="fnOnPageLoad();">
	<html:form action="/gmUDIProductDevelopment.do?method=pdUDISetup">
		<html:hidden property="strOpt" name="frmUDIProductDevelopment" />
		<html:hidden property="inputStr" value="" name="frmUDIProductDevelopment" />
		<html:hidden property="partNumStr" value="" name="frmUDIProductDevelopment" />
		<html:hidden property="printProjectId" value="" name="frmUDIProductDevelopment" />
		<html:hidden property="deletePartStr"  value="" name="frmUDIProductDevelopment"/>
		<html:hidden property="hSearch" value="<%=strSearch%>"/>
		<html:hidden property="msgFl"  value="<%=strMsgFl%>"/>
		<table border="0" class="DtTable1650" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader"><fmtPdUDISetup:message key="LBL_PD_DATA"/></td>
					<td align="right" class=RightDashBoardHeader><fmtPdUDISetup:message key="IMG_ALT_HELP" var="varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
 					title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="RightTableCaption" align="right" HEIGHT="30" width="155"><fmtPdUDISetup:message key="LBL_PROJECT_ID"/>
								ID:&nbsp;</td>
							<td><gmjsp:dropdown controlName="projectId"
									SFFormName="frmUDIProductDevelopment"
									SFSeletedValue="projectId" SFValue="alProjectId" codeId="ID"
									codeName="IDNAME" defaultValue="[Choose One]" tabIndex="1" /></td>
							<td align="right" class="RightTableCaption" HEIGHT="23"
								width="150"><fmtPdUDISetup:message key="LBL_PART_NUMBER"/>:</td>
							<td colspan="1" class="RightText" width="55%">&nbsp;<html:text
									property="partNumber" name="frmUDIProductDevelopment"
									onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
									onblur="changeBgColor(this,'#ffffff');" tabindex="2" size="50"></html:text>
							<select name="Cbo_Search" class="RightText" onFocus="changeBgColor(this,'#AACCE8');"
							 onBlur="changeBgColor(this,'#ffffff');" tabindex="3">
							<option value="0" ><fmtPdUDISetup:message key="LBL_CHOOSE_ONE"/>
							<option value="LIT" ><fmtPdUDISetup:message key="LBL_LITERAL"/>
							<option value="LIKEPRE" ><fmtPdUDISetup:message key="LBL_PREFIX"/>
							<option value="LIKESUF" ><fmtPdUDISetup:message key="LBL_SUFFIX"/></select>
							</td>
						</tr>
						<tr>
							<td colspan="4" class="lline"></td>
						</tr>
						<tr class="Shade" height="30">
							<td align="right" class="RightTableCaption" HEIGHT="23"
								width="150"><fmtPdUDISetup:message key="LBL_PART_DESCRIPTION"/>:</td>
							<td class="RightText" colspan="2"><html:text property="partDesc"
									name="frmUDIProductDevelopment"
									onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
									onblur="changeBgColor(this,'#ffffff');" tabindex="4" size="67"></html:text></td>
							<td>&nbsp;<fmtPdUDISetup:message key="BTN_LOAD" var="varLoad"/><gmjsp:button
									value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" name="Btn_Load"
									gmClass="button" onClick="javascript:fnLoad();" tabindex="5"
									buttonType="Load" /></td>
						</tr>

					</table>
				</td>
			</tr>
			<tr>
				<td colspan="4" class="lline"></td>
			</tr>
			<tr id="trUserManipulation">
				<td class="RightTableCaption" align="left" colspan="3" height="30">
					<div id="rfs_grid" style="display: inline">
						<table cellpadding="1" cellspacing="1" border="0">
							<tr>
								<td class="RightTableCaption">&nbsp;<fmtPdUDISetup:message key="IMG_ALT_COPY" var="varCopy"/><img
									src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()"
									alt="${varCopy}" style="border: none; cursor: hand;" height="14" tabindex="5">
									&nbsp;<fmtPdUDISetup:message key="IMG_ALT_PASTE" var="varPaste"/><img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="${varPaste}"
									style="border: none; cursor: hand;"
									onClick="javascript:pasteToGrid()" height="14" tabindex="6"> &nbsp;<fmtPdUDISetup:message key="IMG_ALT_ADD_ROW" var="varAddRow"/><img
									src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}"
									style="border: none; cursor: hand;"
									onClick="javascript:addRow()" height="14" tabindex="7"> &nbsp;<fmtPdUDISetup:message key="IMG_ALT_REMOVE_ROW" var="varRemoveRow"/><img
									src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}"
									style="border: none; cursor: hand;"
									onClick="javascript:removeSelectedRow()" height="14" tabindex="8"> 
									&nbsp;<fmtPdUDISetup:message key="IMG_ALT_ADD_ROWS" var="varAddRows"/><img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif"
									alt="${varAddRows}"
									style="border: none; cursor: hand;"
									onClick="javascript:addRowFromClipboard()" height="14" tabindex="9">
								</td>
								<td align="right">
								<div id="progress" style="display:none;"><img src="<%=strImagePath%>/success_y.gif" height="15"></div>
								</td>
								<td>
								<div id="msg" style="display:none;color:green;font-weight:bold;"></div> 
								</td>
								
							</tr>
							<tr>
			<td colspan="4" height="17"><font color="#6699FF">&nbsp;
<fmtPdUDISetup:message key="LBL_MAXIMUM"/> <%=strMaxData %><fmtPdUDISetup:message key="LBL_LESS"/> </font></td>
		</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr id="voidLine1" style="display: none;">
				<td colspan="4" class="lline"></td>
			</tr>
			<tr id="voidSuccessfully" style="display: none;" height="40">
			<td colspan="4" align="center"><font color="green" size="20">
<fmtPdUDISetup:message key="LBL_SELECTED"/></font> </td>
			</tr>
			<tr id="saveLine1" style="display: none;">
				<td colspan="4" class="lline"></td>
			</tr>
			<tr height="30" id="loadingTr" style="display: none;">
			<td colspan="4" align="center" class="RightTableText"><div id="saveSuccessfully"></div></td>
			</tr>
			<tr id="trDiv">
				<td colspan="4">
					<div id="partNumData" height="500px" width="1648px"></div>
				</td>
			</tr>
			<tr style="display: none" id="trNoDataFound" height="30">
			<td colspan="4" align="center" class="RightText">
<fmtPdUDISetup:message key="LBL_NO_DATA"/></td>	
			</tr>
			<tr>
				<td colspan="3" class="lline"></td>
			</tr>
			<tr height="30" id="trButton">
				<td align="center"><fmtPdUDISetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}"
						gmClass="button" name="btn_submit" style="width: 6em"
						buttonType="Save" onClick="fnSubmit();" tabindex="10" />&nbsp;&nbsp;&nbsp;<fmtPdUDISetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}"
						gmClass="button" name="btn_Void" style="width: 6em"
						buttonType="Save" onClick="fnVoid();" tabindex="11" />&nbsp;&nbsp;&nbsp;<fmtPdUDISetup:message key="BTN_PRINT" var="varPrint"/><gmjsp:button
						value="${varPrint}" gmClass="button" name="btn_Print" style="width: 6em"
						buttonType="Load" onClick="fnPrint();" tabindex="12" />&nbsp;&nbsp;&nbsp;</td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
