
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ taglib prefix="fmtStaffingCountSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \prodmgmnt\GmStaffingCountSetup.jsp -->
<fmtStaffingCountSetup:setLocale value="<%=strLocale%>"/>
<fmtStaffingCountSetup:setBundle basename="properties.labels.prodmgmnt.GmStaffingCountSetup"/>
<bean:define id="alRequest" name="frmStaffingDetails" property="alStaffingCountDetails" type="java.util.ArrayList"></bean:define>
<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Staffing Count Setup</title>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
</head>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmProject.js"></script> 


<body leftmargin="0" topmargin="0">
<html:form action="/gmPDStaffingCount.do">
<html:hidden property="strOpt"/>
<html:hidden property="prjId"/>
<html:hidden property="hinputString" value=""/>

<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	 <tr>
		 <td colspan="2" width="100%" height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<%if (alRequest.size() > 0) 
					{
					%>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>					
					<tr class="Shade"> 
			            <td colspan="2">
				            <display:table name="requestScope.frmStaffingDetails.alStaffingCountDetails" requestURI="/gmPDStaffingCount.do" excludedParams="haction" class="gmits" id="currentRowObject"  decorator="com.globus.prodmgmnt.displaytag.DTStaffingCountWrapper">
				            <fmtStaffingCountSetup:message key="LBL_ROLE" var="varRole"/><display:column property="ROLENM" title="${varRole}" class="alignleft" style="width:240px" sortable="true"/> 						
				            <fmtStaffingCountSetup:message key="LBL_COMMENTS" var="varComments"/> <display:column property="IMG" title="${varComments}" class="aligncenter" style="width:60px" sortable="true"/>
							<fmtStaffingCountSetup:message key="LBL_MEMEBRS" var="varMembers"/><display:column property="MEMBERS" title="${varMembers}" class="alignleft" style="width:120px"/> 
							</display:table> 						
						</td>
		    	   </tr>
		    	   <tr><td colspan="2" class="LLine" height="1"></td></tr>		    	   
					<tr>
						<td colspan="2" align="center" height="24">&nbsp;
							<fmtStaffingCountSetup:message key="BTN_RESET" var="varReset"/>
							<gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnStaffingCntReset(this.form);"/>&nbsp;&nbsp;
							<fmtStaffingCountSetup:message key="BTN_SUBMIT" var="varSubmit"/>
	                    	<gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnStaffingCntSubmit();"/>&nbsp;&nbsp;                   	
						</td>
					</tr>
					<%} else
					{
			  		%> 
			  		<tr><td colspan="2" class="LLine" height="1"></td></tr> 		
					<tr class="Shade">
			  		<td height="20"><fmtStaffingCountSetup:message key="LBL_NO_STAFFING_COUNT_RECORDS"/></td><td></td>
			  		</tr>
			  		<%} %>
			</table>
		</td>
	</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>