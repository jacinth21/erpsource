<%
/**********************************************************************************
 * File		 		: GmSystemSetMappingDtls.jsp
 * Desc		 		: System Release Mapping  
 * Author           : N RAJA
************************************************************************************/
%>
<!-- prodmgmnt\mrktcollateral\GmSystemSetMappingDtls.jsp -->
<!DOCTYPE html>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtSystemReleaseMapping" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page isELIgnored="false" %>
<fmtSystemReleaseMapping:setLocale value="<%=strLocale%>" />
<fmtSystemReleaseMapping:setBundle 	basename="properties.labels.prodmgmnt.mrktcollateral.GmSystemSetMappingDtls" />
<bean:define id="setId" name="frmSystemDetails" property="setId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="regionId" name="frmSystemDetails" property="regionId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="setGroupType" name="frmSystemDetails" property="setGroupType" scope="request" type="java.lang.String"></bean:define>
<bean:define id="regionName" name="frmSystemDetails" property="regionName" scope="request" type="java.lang.String"></bean:define>
<%
  String strWikiTitle = GmCommonClass.getWikiTitle("SET_MAPPING_DETAILS");  
  String strProdmgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%> 
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: <fmtSystemReleaseMapping:message key="LBL_SET_MAPPING_DTLS"/> </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css"	href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script> 
<script language="JavaScript" src="<%=strProdmgmntJsPath%>/GmSystemSetMappingDtls.js"></script>
<script type="text/javascript">
var setId = '<%=setId%>';
var regionId ='<%=regionId%>';
var strGroupType ='<%=setGroupType%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action ="/gmSystemPublish.do">
<table class="DtTable1000" cellspacing="0" cellpadding="0">
				 <tr>
					<td colspan="3" height="25" class="RightDashBoardHeader">&nbsp;<fmtSystemReleaseMapping:message key="LBL_SET_MAPPING_DTLS"/>&nbsp;- <%=regionName%> </td>
					<td align="right" class=RightDashBoardHeader><fmtSystemReleaseMapping:message key="LBL_HELP" var="varHelp"/> 
					<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
					</td>
				</tr>					
				<tr>
                <td colspan="4">
                <div id="dataGridDiv" height="500px" width="1000px"></div>
               </tr> 
		       <tr><td align="center" colspan="4">
		            <div class='exportlinks' id="DivExportExcel"><fmtSystemReleaseMapping:message key="LBL_EXPORT_OPTIONS" />: <a href="#" onclick="fnDownloadXLS();"><img src='img/ico_file_excel.png' /></a>&nbsp;
					  <a href="#" onclick="fnDownloadXLS();"><fmtSystemReleaseMapping:message key="LBL_EXCEL" /></a>
					</div>
		 	      </td>
		      </tr>    
	          <tr><td colspan="4" align="center" class="RegularText"><div  id="DivNothingMessage"><fmtSystemReleaseMapping:message key="NO_DATA_FOUND"/></div></td></tr>
</table>
</html:form>
</body>
</html>
