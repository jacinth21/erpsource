
 <%
/**********************************************************************************
 * File		 		: GmPartReport.jsp
 * Desc		 		: This screen is used for the Part Number Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!-- \prodmgmnt\GmPartReport.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtPartReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmShipAdd.jsp -->
<fmtPartReport:setLocale value="<%=strLocale%>"/>
<fmtPartReport:setBundle basename="properties.labels.prodmgmnt.GmPartReport"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmParam = (HashMap)request.getAttribute("PARAM");
	ArrayList alProject = new ArrayList();
	ArrayList alLikeOptions = new ArrayList();
	alProject = (ArrayList)request.getAttribute("PROJECTLIST");
	alLikeOptions = (ArrayList)request.getAttribute("LIKEOPTIONS");

	String strProjectId = "";
	String strPartNum = "";
	String strLikeOption = "";
	String strPartDesc = "";
	
	if (hmParam != null)
	{
		strProjectId = GmCommonClass.parseNull((String)hmParam.get("PROJECTID"));
		strPartNum = GmCommonClass.parseNull((String)hmParam.get("PARTNUM"));
		strLikeOption = GmCommonClass.parseNull((String)hmParam.get("LIKEOPTION"));
		strPartDesc = GmCommonClass.parseNull((String)hmParam.get("PARTDESC"));
	}
	
	String strSubCompFl = GmCommonClass.parseZero((String)request.getAttribute("Chk_subcomp_fl"));
	strSubCompFl = strSubCompFl.equals("on")?"checked":"";
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>  
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnSubmit(val)
{
	document.frmProject.hColumn.value = val;
	document.frmProject.submit();
}

function fnCallEdit(val)
{
	document.frmProject.hId.value = val;
	document.frmProject.action = "<%=strServletPath%>/GmProdFrameServlet?hFrom=ProjReport";
	document.frmProject.submit();
}

function fnCallPart(val)
{
	document.frmProject.hId.value = val;
	document.frmProject.hAction = "Part";
	document.frmProject.submit();
}

function fnLoadPart(hpart)
{
	document.frmProject.Txt_PartNum.value = hpart;
	document.frmProject.hAction.value = 'Lookup';
	document.frmProject.action = '<%=strServletPath%>/GmPartNumServlet';
	document.frmProject.submit();
}

function fnGo()
{
	document.frmProject.hAction.value = "Part";
	fnStartProgress('Y');
	document.frmProject.submit();
}

function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnGo();
		}
}
function fnPartDetails(partNum)
{
	windowOpener('<%=strServletPath%>/GmPartNumServlet?hAction=LoadPartDetail&partNumber='+encodeURIComponent(partNum),"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=760,height=600");
}
</script>
</HEAD>

<BODY  onkeyup="fnEnter();"  leftmargin="20" topmargin="10">
<FORM name="frmProject" method="POST" action="<%=strServletPath%>/GmProdReportServlet">
<input type="hidden" name="hAction" value="">
	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td class="RightDashBoardHeader" height="28">&nbsp;<fmtPartReport:message key="LBL_PART_NUMBER_REPORT"/></td>
		</tr>
		<tr>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="RightTableCaption"  align = "right" HEIGHT="30" width="80"> <fmtPartReport:message key="LBL_PROJECT_ID"/>:&nbsp; </td>
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<td width="120"> <gmjsp:dropdown controlName="Cbo_Project"  seletedValue="<%=strProjectId%>" 	
								tabIndex="1"   value="<%=alProject%>" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]"  />
						</td>
						<td class="RightTableCaption"  align = "right" HEIGHT="30" width="100"> &nbsp;<fmtPartReport:message key="LBL_PART_NUMBER"/>:&nbsp; </td>
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<td> <input type="text" size="30" value="<%=strPartNum%>" name="Txt_PartNum" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=2>
							&nbsp;&nbsp;<gmjsp:dropdown controlName="Cbo_Search"  seletedValue="<%= strLikeOption%>" 	
									tabIndex="3"  value="<%=alLikeOptions%>" codeId = "CODEID" codeName = "CODENM" 	/>
									 <fmtPartReport:message key="BTN_LOAD" var="varLoad"/>
									&nbsp;&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" buttonType="Load" onClick="fnGo();"  tabindex="4"/>&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
						<td align="center" rowspan="2"></td>
					</tr>
					<tr>
						<td class="RightTableCaption"  align = "right" HEIGHT="30" width="80"> &nbsp;<fmtPartReport:message key="LBL_DESCRIPTION"/>:&nbsp; </td>
						<td> <input type="text" size="30" value="<%=strPartDesc%>" name="Txt_PartDesc" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=5>
						</td>
						<td>&nbsp;</td>
						<td class="RightTableCaption"  align = "left" HEIGHT="30" width="180"> 
						<input type="checkbox"  <%=strSubCompFl%>   name="Chk_subcomp_fl" tabindex="6" >
						&nbsp;<fmtPartReport:message key="LBL_SUBCOMPONENT"/></td>
						
						
					</tr>
				</table>
			</td>
		</tr>		
		<tr><td height="1" bgcolor="#666666"></td></tr>	
		<tr>
			<td>
				<display:table name="alReturn" requestURI="/GmProdReportServlet" class="its" id="currentRowObject" export="true" decorator="com.globus.prodmgmnt.displaytag.DTPartReportWrapper">
				<display:setProperty name="export.excel.filename" value="PartReport.xls" /> 
				<fmtPartReport:message key="LBL_PART_NUMBER" var="varPartNumber"/><display:column property="PARTNUM" title="${varPartNumber}" sortable="true"  />
				<fmtPartReport:message key="LBL_DESCRIPTION" var="varDescription"/><display:column property="PDESC" escapeXml="true" title="${varDescription}" sortable="true"  />
				<fmtPartReport:message key="LBL_PART_FAM" var="varPartfam"/><display:column property="PFLYNM" escapeXml="true" title="${varPartfam}" sortable="true"  />
				<fmtPartReport:message key="LBL_PROJ_ID" var="varProjID"/><display:column property="PROJID" title="${varProjID}" sortable="true"  />
				<fmtPartReport:message key="LBL_PROJ_NAME" var="varProjNm"/><display:column property="PROJNM"   title="${varProjNm}" sortable="true"  />
				<fmtPartReport:message key="LBL_DRAW" var="varDraw"/><display:column property="DRAW" title="${varDraw}" sortable="true" class="aligncenter" />
				<fmtPartReport:message key="LBL_DRAW_REV" var="varDrawRev"/><display:column property="REV" title="${varDrawRev}" class="aligncenter" sortable="true" />
				<fmtPartReport:message key="LBL_PRODUCT_MATERIAL" var="varProductMaterial"/><display:column property="PMATNM" title="${varProductMaterial}" class="alignleft" sortable="true" />
			    <fmtPartReport:message key="LBL_MATERIAL_SPEC" var="varMaterailSpec"/>	<display:column property="MSPECNM" title="${varMaterailSpec}" sortable="true" class="aligncenter" />
			    <fmtPartReport:message key="LBL_MATERIAL_CERT" var="varMaterialCert"/>	<display:column property="MATCERT" title="${varMaterialCert}" sortable="true" class="aligncenter" />
			  	<fmtPartReport:message key="LBL_COMPL_CERT" var="varComplCert"/><display:column property="COMPCERT" title="${varComplCert}" sortable="true" class="aligncenter" />
			  	<fmtPartReport:message key="LBL_INSPECT_REV" var="varInspectRev"/><display:column property="INSPREV" title="${varInspectRev}" sortable="true" class="aligncenter" />
			  	<fmtPartReport:message key="LBL_INSERT_ID" var="varInsertId"/><display:column property="INSERTID" title="${varInsertId}" sortable="true" class="aligncenter" />
			  	<fmtPartReport:message key="LBL_STATUS" var="varStatus"/><display:column property="STATUSMSG" title="${varStatus}" sortable="true" class="aligncenter" />
			  	<fmtPartReport:message key="LBL_RELEASE_FOR_SALE" var="varReleaseForSale"/><display:column property="RELFORSALE" title="${varReleaseForSale}" sortable="true" class="aligncenter" />
			  	<fmtPartReport:message key="LBL_RELEASE_FOR_SALE_EU" var="varReleaseForSale_EU"/><display:column property="RELFORSALE_EU" title="${varReleaseForSale_EU}" sortable="true" class="aligncenter" />
			  	<fmtPartReport:message key="LBL_SALES_GROUP" var="varSalesGroup"/><display:column property="SALESGROUPNM" title="${varSalesGroup}" sortable="true" class="alignleft" />
			  	<fmtPartReport:message key="LBL_NOTI_NAPPI" var="varNotiNappiCode"/><display:column property="NAPPICODE" title="${varNotiNappiCode}" sortable="true" class="alignleft" />
			</display:table> 
			</td>
		</tr>
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
