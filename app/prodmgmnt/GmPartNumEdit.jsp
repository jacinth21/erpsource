 <%
/**********************************************************************************
 * File		 		: GmVendorPriceSetup.jsp
 * Desc		 		: This screen is used for the Vendor Price Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!-- prodmgmnt\GmPartNumEdit.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPartNumEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartNumEdit:setLocale value="<%=strLocale%>"/>
<fmtPartNumEdit:setBundle basename="properties.labels.prodmgmnt.GmPartNumEdit"/>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)session.getAttribute("hmReturn");

	String strhAction = (String)request.getAttribute("hAction");
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strProjId =	"";
	String strProjNm = "";
	String strPartNum = "";
	String strPartNumDesc = "";

	ArrayList alProjList = new ArrayList();
	ArrayList alDataList = new ArrayList();
	ArrayList alMaterialList = new ArrayList();
	ArrayList alPartList = new ArrayList();	
	int intDataSize = 0;

	if (hmReturn != null)
	{
		alProjList = (ArrayList)hmReturn.get("PROJECTS");
		strProjId = (String)request.getAttribute("hProjId") == null?"":(String)request.getAttribute("hProjId");
		strPartNum = (String)request.getAttribute("hPartNum") == null?"":(String)request.getAttribute("hPartNum");
		if (strhAction.equals("Part")||strhAction.equals("Go"))
		{
			alPartList = (ArrayList)hmReturn.get("PARTNUMS");
		}		
		if (strhAction.equals("Go"))
		{
			alDataList = (ArrayList)hmReturn.get("DATALIST");
			alMaterialList = (ArrayList)hmReturn.get("MATSPEC");
			intDataSize = alDataList.size();
		}
	}


	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Assembly Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<SCRIPT language="JavaScript" src="<%=strJsPath%>/ListAlign.js"> </SCRIPT>
<SCRIPT language="JavaScript" src="<%=strJsPath%>/ListConfig.js"> </SCRIPT>
<SCRIPT language="JavaScript" src="<%=strJsPath%>/EditPart.js"> </SCRIPT>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script>
function fnCallProject(val)
{
	document.frmVendor.hAction.value = 'Part';
  	document.frmVendor.submit();
}
function fnGo()
{
	document.frmVendor.hAction.value = 'Go';
	document.frmVendor.submit();
}

</script>

<SCRIPT>
	var TotalRows = 0;
	foDBArray = new Array(1);
	TotalRows = <%=intDataSize%>;
<%
	HashMap hmLoop = new HashMap();
	if (intDataSize > 0)
	{
	   for (int i=0;i<intDataSize;i++)
	   {
			hmLoop = (HashMap)alDataList.get(i);
%>
	foDBArray[<%=i%>] = new Array("<%=hmLoop.get("PNUM")%>","<%=hmLoop.get("PDESC")%>","<%=hmLoop.get("MSPEC")%>","<%=hmLoop.get("DRAW")%>","<%=hmLoop.get("REV")%>","U","<%=hmLoop.get("SEQID")%>");
<%
	   }
	}
%>

</SCRIPT>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmPartNumEditServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<INPUT type="hidden" name="inputString" value="">
	<table border="0" width="650" cellspacing="0" cellpadding="0">
		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartNumEdit:message key="LBL_PART_NUMBER"/></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr>
			<td width="648" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" colspan="2" HEIGHT="40">&nbsp;<fmtPartNumEdit:message key="LBL_PROJECT_NAME"/>:</td>
						&nbsp;<select name="Cbo_Proj" id="Region" class="RightText" tabindex="3" onChange="javascript:fnCallProject(this);" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtPartNumEdit:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alProjList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alProjList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strProjId.equals(strCodeID)?"selected":"";
						strProjNm = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeID%>::<%=strProjNm%></option>
<%
			  		}
%>
						</select>
<%
					if (strhAction.equals("Part") || strhAction.equals("Go"))
					{
%>
					Part Number: <select name="Cbo_Part" id="Region" class="RightText" tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="fnGo();"><option value="0" ><fmtPartNumEdit:message key="LBL_CHOOSE_ONE"/>
<%
			  			intSize = alPartList.size();
						hcboVal = new HashMap();
				  		for (int i=0;i<intSize;i++)
				  		{
				  			hcboVal = (HashMap)alPartList.get(i);
			  				strCodeID = (String)hcboVal.get("ID");
							if (strPartNum.equals(strCodeID))
							{
								strSelected = "selected";
								strPartNumDesc = (String)hcboVal.get("NAME");
							}
							else
							{
								strSelected = "";
							}
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("ID")%></option>
<%
			  			}
%>
						</select> &nbsp;&nbsp;
						<!--<gmjsp:button value="&nbsp;Go&nbsp;" gmClass="button" buttonType="Save" onClick="fnGo();" tabindex="16" />&nbsp;-->
<%
			  		}
%>
						</td>
					</tr>
<%
					if (strhAction.equals("Go"))
					{
%>

					<tr>
						<td class="RightText" height="30">
						&nbsp;<fmtPartNumEdit:message key="LBL_PART_DESCRIPTION"/>: <%=GmCommonClass.getStringWithTM(strPartNumDesc)%><br>

							<TABLE cellpadding="1" cellspacing="0" border="0" width="100%">
								<tr><td colspan="5" bgcolor="#eeeeee" height="1"></td></tr>
								<tr>
									<td colspan="5" class="RightText" height="30">
									&nbsp;<fmtPartNumEdit:message key="LBL_ASSEMBLY_SUB"/>
									<input type="checkbox" name="Chk_Con">
									</td>
								</tr>								
								<tr><td colspan="5" class="Line" height="1"></td></tr>								
								<TR class="Shade">
									<TD class="RightText"><fmtPartNumEdit:message key="LBL_PART_NUM"/></TD>
									<TD class="RightText"><fmtPartNumEdit:message key="LBL_DESCRIPTION"/></TD>
									<TD class="RightText"><fmtPartNumEdit:message key="LBL_MATERIAL_SPEC"/></TD>
									<TD class="RightText" width="100"><fmtPartNumEdit:message key="LBL_DRAWING"/></TD>
									<TD class="RightText" width="100"><fmtPartNumEdit:message key="LBL_REV"/></TD>
								</TR>
								<TR>
									<td width="100" valign="top"><input type="text" size="10" class="InputArea" name="Txt_Part"></td>
									<td  valign="top"><input type="text" size="40" class="InputArea" name="Txt_Desc"></td>
									<TD valign="top" >
										<SELECT name="Cbo_Mat" class="InputArea" style="WIDTH: 120px;">
										<option value="0" >[Choose One]
<%
			  			intSize = alMaterialList.size();
						hcboVal = new HashMap();
				  		for (int i=0;i<intSize;i++)
				  		{
				  			hcboVal = (HashMap)alMaterialList.get(i);
			  				strCodeID = (String)hcboVal.get("CODEID");
%>
							<option value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  			}
%>
										</SELECT>
									</TD>
									<td  valign="top"><input type="text" size="8" class="InputArea" name="Txt_Draw"></td>
									<td  valign="top"><input type="text" size="3" class="InputArea" name="Txt_Rev"></td>
								<TR>
									<TD colspan="5" ><select name="Lst_Pay" size="10" style="white-space: pre;font-family: MONOSPACE ;HEIGHT: 100px;WIDTH: 600px" onChange="javascript:Selected(this)"></select>
									</TD>
								</TR>
								<TR>
									<TD colspan="5" align="center">
									<fmtPartNumEdit:message key="BTN_ADD" var="varAdd"/><gmjsp:button width="60" name="Btn_Add" value="${varAdd}" gmClass="InputArea" buttonType="Save"  onClick ="fnAddOption(document.frmVendor.Lst_Pay)"  style="WIDTH: 60px; HEIGHT: 16px" />&nbsp;
									<fmtPartNumEdit:message key="BTN_EDIT" var="varEdit"/><gmjsp:button width="60" gmClass="InputArea" name="Btn_Edit" value="${varEdit}" buttonType="Save" onClick ="fnUpdateOption(document.frmVendor.Lst_Pay)"  style="WIDTH: 60px; HEIGHT: 16px" />&nbsp;
									<fmtPartNumEdit:message key="BTN_DELETE" var="varDelete"/><gmjsp:button width="60" gmClass="InputArea" name="Btn_Delete" value="${varDelete}" buttonType="Save" onClick ="fnDeleteOption(document.frmVendor.Lst_Pay)"  style="WIDTH: 60px; HEIGHT: 16px" />
									</TD>
								</TR>
							</table>
						</td>
				   </tr>
				   <tr>
						<td  align="center" height="35">
							<fmtPartNumEdit:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" buttonType="Save" onClick="fnSubmit();" tabindex="16" />&nbsp;
						</td>
					</tr>
<%
					}
%>
				</table>
			</td>
		</tr>
		<tr>
			<td height="1" bgcolor="#666666"></td>
		</tr>
    </table>
<SCRIPT>
<%
if (intDataSize == 0)
{
%>
	Initial_Setting();
<%
}else{
%>
	Screen_Load();
<%}%>
</SCRIPT>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
