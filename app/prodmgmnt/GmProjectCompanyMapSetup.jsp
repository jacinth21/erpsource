<%
/**********************************************************************************
 * File		 		: GmProjectCompanyMapSetup.jsp
 * Desc		 		: This screen is used to fetch project company mapping details
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- prodmgmnt\GmProjectCompanyMapSetup.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtProjectCompanyMapSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page isELIgnored="false" %>

<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("PROJECT_SETUP_MAPPING");
%>
<fmtProjectCompanyMapSetup:setLocale value="<%=strLocale%>"/>
<fmtProjectCompanyMapSetup:setBundle basename="properties.labels.prodmgmnt.GmProjectCompanyMapSetup"/>

<bean:define id="gridData" name="frmProjectDetails" property="gridXmlData" type="java.lang.String"> </bean:define> 
<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Project - Company Mapping</title>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmProjectCompanyMapSetup.js"></script>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	// to set the row id
	var RowCount = 10;
</script>
</HEAD>
<BODY onload="fnOnPageLoad();">
<html:form action="/gmPDProjectSetup.do?method=loadProjectCompanyMapSetup">
<html:hidden property="strOpt" value=""/>
<html:hidden property="prjId"/>
<html:hidden property="hinputString" value=""/>
<table border="0" height="300px" width="660px" cellspacing="0" cellpadding="0">
		
	<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;<fmtProjectCompanyMapSetup:message key="LBL_PART_PROJECT_MAPPING"/></td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtProjectCompanyMapSetup:message key="IMG_ALT_HELP" var="varHelp"/>
					<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
	</tr>
	<tr  class="evenshade">
	<td class="RightTableCaption" HEIGHT="30" align="right" width="200">
	<fmtProjectCompanyMapSetup:message key="LBL_COMPANY" var="varCompany"/>
						<gmjsp:label type="MandatoryText" SFLblControlName="${varCompany}:" td="false" /></td>&nbsp;
				<td >&nbsp;<gmjsp:dropdown controlName="projectReleaseCompanyId" SFFormName="frmProjectDetails" SFSeletedValue="projectReleaseCompanyId" SFValue="alParentCompany"
						 codeId = "COMPANYID"  codeName = "COMPANYNM" defaultValue= "[Choose One]" onChange="fnChangeCompany (this);"/>
						</td>
	</tr>
	<tr>	 
		<td colspan ="2">
			<div id="dataGridDiv"  style="width:680px;height:300px;"></div>
		</td>
		
		</tr>
	<tr>
		<td colspan="2" height="40" align="center">
	   		<div colspan="2" id="divHideButton">
	   		<fmtProjectCompanyMapSetup:message key="BTN_SUBMIT" var="varSubmit"/>
	   		<logic:equal name="frmProjectDetails" property="strEnableSubmit" value="false">
			<gmjsp:button style="width: 5em"  value="${varSubmit}" name="Btn_Submit" buttonType="Save"  gmClass="button" onClick="fnSubmit();" disabled="true" />&nbsp;
			</logic:equal>
			<logic:equal name="frmProjectDetails" property="strEnableSubmit" value="true">
			<gmjsp:button style="width: 5em"  value="${varSubmit}" name="Btn_Submit" buttonType="Save"  gmClass="button" onClick="fnSubmit();" />&nbsp;
			</logic:equal>
			</div>
		</td>
	</tr>				   	
</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>