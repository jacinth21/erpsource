
<%
/**********************************************************************************
 * File		 		: GmArtifacts.jsp
 * Desc		 		: Screen used to upload files for Product Catalog
 * Version	 		: 1.0
 * author			: Anilkumar
************************************************************************************/
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtArtifacts" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\mrktcollateral\GmArtifacts.jsp -->
<fmtArtifacts:setLocale value="<%=strLocale%>"/>
<fmtArtifacts:setBundle basename="properties.labels.prodmgmnt.mrktcollateral.GmArtifacts"/>

<bean:define id="strOpt" name="frmArtifacts" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="strSubmitAccess" name="frmArtifacts" property="submitAccess" type="java.lang.String"> </bean:define>
<bean:define id="xmlStringData" name="frmArtifacts" property="xmlStringData" type="java.lang.String"> </bean:define>
<bean:define id="strUploadType" name="frmArtifacts" property="uploadType" type="java.lang.String"> </bean:define>
<bean:define id="strRefId" name="frmArtifacts" property="refId" type="java.lang.String"> </bean:define>
<bean:define id="alUploadTypes" name="frmArtifacts" property="alUploadTypes" type="java.util.ArrayList"> </bean:define>
<bean:define id="alArtifactsType" name="frmArtifacts" property="alArtifactsType" type="java.util.ArrayList"> </bean:define>
<bean:define id="alSubType" name="frmArtifacts" property="alSubType" type="java.util.ArrayList"> </bean:define>
<bean:define id="hmKey" name="frmArtifacts" property="hmKey" type="java.util.HashMap"> </bean:define>
<bean:define id="systemKeywords" name="frmArtifacts" property="systemKeywords" type="java.lang.String"> </bean:define>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("SHARE_ARTIFACT");
 
  String strProdmgmntMrktcollateralJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_MRKTCOLLATERAL");

	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
%>
 
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Shareable Artifacts</title>

<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strProdmgmntMrktcollateralJsPath%>/GmArtifacts.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<script language="JavaScript">
	var objGridData;
	objGridData = '<%=xmlStringData%>';	
	 var artifactsTypeLen = <%=alArtifactsType.size()%>;
	 var subTypeLen = <%=alSubType.size()%>;
	<%
		HashMap hmSubVal = new HashMap();
		for (int i=0;i<alSubType.size();i++)
		{
		  hmSubVal = (HashMap)alSubType.get(i);
	%>
	var	arrSubType<%=i%> = "<%=hmSubVal.get("CODEID")%>,<%=hmSubVal.get("CODENM")%>,<%=hmSubVal.get("CODENMALT")%>" ;
	<%
		}
	%>
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();" onkeypress="fnEnter();">
<html:form action="/gmShareArtifacts.do">
<html:hidden property="strOpt" name="frmArtifacts"/>
<html:hidden property="inputString" />
<html:hidden property="submitAccess" />
<html:hidden property="keywordType" value="103108"/>
<html:hidden property="keywordAccess" />


	<table border="0" class="DtTable950" cellspacing="0" cellpadding="1" width="100%">
				<tr class="shade" style="width:100%">
						<td height="25" class="RightDashBoardHeader" colspan="4">&nbsp;<fmtArtifacts:message key="LBL_SHAREABLE_ARTIFACTS"/></td>
						<td  height="25" class="RightDashBoardHeader">
							<fmtArtifacts:message key="IMG_ALT_HELP" var = "varHelp"/>
							<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
						</td>
				</tr>
				<tr><td class="LLine" colspan="5"></td></tr>
				<tr>	
					<td colspan="5">
					    <jsp:include page="/prodmgmnt/mrktcollateral/GmIncludeUploadType.jsp" >
						<jsp:param name="FORMNAME" value="frmArtifacts" />
						</jsp:include>
					</td>
				</tr>
			    <tr class="shade">
			  		<td class="RightTableCaption" align="right" height="30" width="150">&nbsp;<fmtArtifacts:message key="LBL_ARTIFACT_TYPE"/>:</td>					
					<td width="200">&nbsp;<gmjsp:dropdown controlName="artifactsType" onChange="javascript:fnPopulateSubType(this, 'ARTIFACT');" SFFormName="frmArtifacts"
							SFSeletedValue="artifactsType" SFValue="alArtifactsType" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]" width="150" />
					</td>
					<td class="RightTableCaption" align="right" height="30" width="100" >&nbsp;<fmtArtifacts:message key="LBL_SUB_TYPE"/>:</td>
				    <td width="300">&nbsp;<gmjsp:dropdown controlName="subType"  SFFormName="frmArtifacts" SFSeletedValue="subType" SFValue="alSubType"
					       codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"  width="250"/>
			        </td>
			         
				    <td class="RightTableCaption" align="left" height="30" width="200">
				       <fmtArtifacts:message key="BTN_LOAD" var="varLoad"/> <gmjsp:button name="Btn_Load" value="&nbsp;${varLoad}&nbsp;" style="height: 23px; width: 6em;" gmClass="Button" buttonType="Load" onClick="fnReLoad();" />
			    </tr>
			    <%if(strUploadType.equals("103092")&&!strRefId.equals("")&&!strRefId.equals("0")){ %>
			    <tr>
				<td class="RightTableCaption" align="right" height="30" width="150" >
					<logic:equal name="frmArtifacts" property="keywordAccess" value="Y">
						<a href="#" onClick="fnKeyword('<%=strRefId%>','103108','<%=strRefId%>');"><fmtArtifacts:message key="IMG_ALT_CLICK" var = "varClick"/><img  width="39" height="19" alt="${varClick}"  src="<%=strImagePath%>/edit.jpg"/ border="0"></a>
					</logic:equal>
					<fmtArtifacts:message key="LBL_KEYWORDS"/>:
					</td>
					<td colspan="4">&nbsp;<textarea cols="70" readonly="readonly" rows="2"><%=systemKeywords%></textarea>
					</td>
				</tr>
			    
		        <%}if(xmlStringData.indexOf("</row>")!=-1){ %>
				<tr>
					<td colspan="7">
						<div  id="divGrid" style="" height="400px" width="950px"></div>
					</td>
				</tr>
				<tr><td colspan="7" align="center" height="25">
						<div class='exportlinks'><fmtArtifacts:message key="DIV_EXPORT_OPT"/>:<img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"><fmtArtifacts:message key="DIV_EXCEL"/></a></div>
					</td>                 
				</tr>
				<%if(strUploadType.equals("103092") && strSubmitAccess.equals("Y")){ %>	
				<tr>
					<td height="1" colspan="7" class="LLine"></td>
				</tr>
				<tr>	
					<td class="RightTableCaption" colspan="7" align="center" height="30">
			   			<fmtArtifacts:message key="BTN_SUBMIT" var="varSubmit"/> <gmjsp:button name="Btn_Submit" value="&nbsp;${varSubmit}&nbsp;" style="height: 23px; width: 6em;" gmClass="Button" buttonType="Save" onClick="fnSubmit(this.form);" />
					</td>	
				</tr>
			    <%}}else{%>
				<tr>
					<td colspan="5" height="25" class="LLine"></td>
				</tr>
				<tr >
				<td colspan="9" align="center" class="RightText" ><fmtArtifacts:message key="LBL_NO_DATA"/>
				</td>
				</tr>
			 	<%}%>
			 
</table>
			
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>