<%
/**********************************************************************************
 * File		 		: GmIncludeUploadType.jsp
 * Desc		 		: used to add upload type and their types 
 * Version	 		: 1.0
 * author			: Anilkumar
************************************************************************************/
%>


<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtIncludeUploadType" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\mrktcollateral\GmIncludeUploadType.jsp  -->
<fmtIncludeUploadType:setLocale value="<%=strLocale%>"/>
<fmtIncludeUploadType:setBundle basename="properties.labels.prodmgmnt.mrktcollateral.GmIncludeUploadType"/>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%
String strFormName = "frmArtifacts";
strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<bean:define id="strOpt" name='<%=strFormName%>' property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="strUploadType" name='<%=strFormName%>'  property="uploadType" type="java.lang.String"> </bean:define>
<bean:define id="hmKey" name='<%=strFormName%>'  property="hmKey" type="java.util.HashMap"> </bean:define>
<%
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");

	String strCodeId=(String)hmKey.get("ID");
	String strCodeNm=(String)hmKey.get("NAME");
	String strLableNm=(String)hmKey.get("LABEL");
%>
 
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Shareable Artifacts</title>

<script language="JavaScript"> var refLabel='<%=strLableNm%>'</script>

</HEAD>
<BODY leftmargin="20" topmargin="2">
	<table border="0" width="100%" cellspacing="0" cellpadding="0">		
		<tr>
			<td class="RightTableCaption" align="right" height="30" width="150"><font color="red">*</font>&nbsp;<fmtIncludeUploadType:message key="LBL_UPLOAD_TYPE"/>:</td>					
			<td colspan="1" width="200">&nbsp;<gmjsp:dropdown controlName="uploadType" onChange="fnLoad();" SFFormName='<%=strFormName%>'
				SFSeletedValue="uploadType" SFValue="alUploadTypes" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]" width="150" />
			</td>
							  
			<td class="RightTableCaption" align="right" height="30" width="100" ><font color="red">*</font>&nbsp;<%=strLableNm%>:</td>
			<td width="500" colspan="2">&nbsp;<%if(strUploadType.equals("103092") || strUploadType.equals("103094")){ %><gmjsp:dropdown controlName="refId"
			    SFFormName='<%=strFormName%>' SFSeletedValue="refId" SFValue="alRefId"  width="400" codeId="<%=strCodeId%>" codeName="<%=strCodeNm%>" defaultValue="[Choose One]"/>
				 <%}else if(strUploadType.equals("103090") || strUploadType.equals("103091")){%>
				<html:text property="refId"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
				<%}%>
			</td>
		</tr>		
	</table>
</BODY>
</HTML>