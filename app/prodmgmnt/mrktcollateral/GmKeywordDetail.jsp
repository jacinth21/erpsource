<%
/**********************************************************************************
 * File		 		: GmArtifacts.jsp
 * Desc		 		: Screen used to upload files for Product Catalog
 * Version	 		: 1.0
 * author			: Anilkumar
************************************************************************************/
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtKeywordDetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- prodmgmnt\mrktcollateral\GmKeywordDetail.jsp -->
<fmtKeywordDetail:setLocale value="<%=strLocale%>"/>
<fmtKeywordDetail:setBundle basename="properties.labels.prodmgmnt.mrktcollateral.GmKeywordDetail"/>
<bean:define id="xmlStringData" name="frmKeywordDetails" property="xmlStringData" type="java.lang.String"> </bean:define>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("KEYWORD_DETAILS");
	String strProdmgmntMrktcollateralJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_MRKTCOLLATERAL");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
%>
 
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Keyword Details</title>
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strProdmgmntMrktcollateralJsPath%>/GmKeywordDetail.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<script language="JavaScript">
	var objGridData;
	objGridData = '<%=xmlStringData%>';	
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();" >
<html:form action="/gmKeywordDtl.do">
<html:hidden property="strOpt"/>
<html:hidden property="inputString" />
<html:hidden property="type" />
<html:hidden property="refId" />
<html:hidden property="systemId" />
<input type="hidden" name="hDisplayNm"/>
<input type="hidden" name="hRedirectURL"/>
<html:hidden property="rows" />


	<table border="0"  class="DtTable700" cellspacing="0" cellpadding="0" width="100%">
				<tr class="shade" style="width:100%"  >
						<td height="25" class="RightDashBoardHeader" colspan = "1">&nbsp; 
<fmtKeywordDetail:message key="LBL_KEYWORD_DETAILS"/></td>
						<td  height="25" class="RightDashBoardHeader">
						<fmtKeywordDetail:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
				<tr >
					<td colspan="2">
						<div  id="KEYWORDDATA" style="" height="350px" width="700px"></div>
					</td>
				</tr>
					<tr>
					<td height="1"  class="LLine"></td>
				</tr>
				<tr >	
					<td class="RightTableCaption"  align="center" height="30" colspan="2">
			   			<fmtKeywordDetail:message key="BTN_SUBMIT" var="varSubmit"/> <gmjsp:button align="right"   name="Btn_Submit" value="&nbsp;${varSubmit}&nbsp;" style="height:23px;width:6em;" gmClass="Button" buttonType="Save" onClick="fnSubmit();" />&nbsp;&nbsp;
			   			<fmtKeywordDetail:message key="BTN_VOID" var="varVoid"/> <gmjsp:button  align="left"   name="Btn_Void" value="&nbsp;${varVoid}&nbsp;" style="height:23px;width:6em;" gmClass="Button" buttonType="Save" onClick="fnVoid();" />&nbsp;&nbsp;
			   			<fmtKeywordDetail:message key="BTN_CLOSE" var="varClose"/> <gmjsp:button  align="left"   name="Btn_Close" value="&nbsp;${varClose}&nbsp;" style="height:23px;width:6em;" gmClass="Button" buttonType="Load"  onClick="fnClose();" />
					</td>	
				</tr>
	</table>
			
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>