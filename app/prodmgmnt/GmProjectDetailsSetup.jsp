<!-- prodmgmnt\GmProjectDetailsSetup.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtProjectDetailsSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtProjectDetailsSetup:setLocale value="<%=strLocale%>"/>
<fmtProjectDetailsSetup:setBundle basename="properties.labels.prodmgmnt.GmProjectDetailsSetup"/>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<bean:define id="alDevClass" name="frmProjectDetails" property="alDevClass" type="java.util.ArrayList"></bean:define>
<bean:define id="alPrjGrpList" name="frmProjectDetails" property="alPrjGrpList" type="java.util.ArrayList"></bean:define>
<bean:define id="alPrjSgmtList" name="frmProjectDetails" property="alPrjSgmtList" type="java.util.ArrayList"></bean:define>
<bean:define id="alPrjTechList" name="frmProjectDetails" property="alPrjTechList" type="java.util.ArrayList"></bean:define>
<bean:define id="projectID" name="frmProjectDetails" property="projectID" type="java.lang.String"></bean:define>
<bean:define id="prjStatus" name="frmProjectDetails" property="prjStatus" type="java.lang.String"></bean:define>
<%

String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");

String strCollapseImage = "/images/minus.gif";
HashMap hmPrjDevClassMap = new HashMap();
hmPrjDevClassMap.put("ID","CODEID");
hmPrjDevClassMap.put("PID","CODEID");
hmPrjDevClassMap.put("NM","CODENM");

HashMap hmPrjGrpMap = new HashMap();
hmPrjGrpMap.put("ID","CODEID");
hmPrjGrpMap.put("PID","CODEID");
hmPrjGrpMap.put("NM","CODENM");

HashMap hmPrjSgmMap = new HashMap();
hmPrjSgmMap.put("ID","CODEID");
hmPrjSgmMap.put("PID","CODEID");
hmPrjSgmMap.put("NM","CODENM");

HashMap hmPrjTechMap = new HashMap();
hmPrjTechMap.put("ID","CODEID");
hmPrjTechMap.put("PID","CODEID");
hmPrjTechMap.put("NM","CODENM");

boolean strProjectDisable=false;
if(!prjStatus.equals("Initiated")&&!prjStatus.equals("") ){  //Disable the project id text box
 strProjectDisable=true;
 } 
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Party Setup</title>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmProject.js"></script>

</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmPDProjectSetup.do?method=loadProjectInformation">
<html:hidden property="prjStatusId"/>
<html:hidden property="prjStatus"/>
<html:hidden property="chkPrjDevClass"/>
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="PROJS"/>
<html:hidden property="deviceClassGrpId"/>
<html:hidden property="groupGrpId"/>
<html:hidden property="segmentGrpId"/>
<html:hidden property="techniqueGrpId"/>
<html:hidden property="hDeviceIds" styleId="hDeviceIds"/>
<html:hidden property="hGroupIds" styleId="hGroupIds"/>
<html:hidden property="hSegmentIds" styleId="hSegmentIds"/>
<html:hidden property="hTechniqueIds" styleId="hTechniqueIds"/>
<html:hidden property="projAttrInputStr"/>
<html:hidden property="attrTypeInputStr"/>



<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2" class="LLine" height="1"></td>
	</tr>
	 <tr>
		 <td colspan="2" width="100%" height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				<tr class="ShadeRightTableCaption">
					<td height="25" colspan="2" ><a href="javascript:fnShowFilters('tabProject');" tabindex="-1" style="text-decoration:none; color:black" >
						<IMG id="tabProjectimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtProjectDetailsSetup:message key="LBL_PROJECT_DETAILS"/></a>
					</td>
				</tr>
				<tr>
				<td colspan="2">
					<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0" id="tabProject">
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr>
							<td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;<fmtProjectDetailsSetup:message key="LBL_PROJECT_NAME"/>:&nbsp;</td>
							<td width="80%"><html:text property="prjNm" maxlength="72" size="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
								onblur="changeBgColor(this,'#ffffff');" />
							</td>
						</tr>
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr class="Shade">
		                	<td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;<fmtProjectDetailsSetup:message key="LBL_PROJECT_ID"/>:&nbsp;</td> 
		                	
		                    <td width="80%"><html:text property="projectID"  size="10" maxlength="10" style="text-transform:uppercase" onfocus="changeBgColor(this,'#AACCE8');fnPRJIDClearDiv();" styleClass="InputArea"
								onblur="changeBgColor(this,'#ffffff');fnCheckTxtFldPattern(this.form);fnCheckProjectID(this.form);" disabled="<%=strProjectDisable%>"/>
								
                           <span id="DivShowPRJIDAvail" style="vertical-align:middle; display: none;"> <img height="24" width="25" title="Project Id available" src="<%=strImagePath%>/success.gif"></img></span>
                           <span id="DivShowPRJIDExists" style="vertical-align:middle; display: none;"><img title="Project Id already exists" src="<%=strImagePath%>/delete.gif"></img>&nbsp;<a href="javascript:fnLookupProjectInfo()"><img height="15" border="0" title="Project Details" src="<%=strImagePath%>/location.png"></img></a></span>			
								
							</td>								

		                </tr>						
		                <tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr>
							<td class="RightTableCaption" align="right" height="320"><fmtProjectDetailsSetup:message key="LBL_DESCRIPTION"/>:&nbsp;</td>
							<td width="40%"><textarea name="prjDesc"  class="HtmlTextArea" onfocus="changeBgColor(this,'#AACCE8');" 
									onblur="changeBgColor(this,'#ffffff');"  rows="3" cols="50"><bean:write name="frmProjectDetails" property="prjDesc"/></textarea> 
							</td>
						</tr>
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr class="Shade">
		                	<td class="RightTableCaption" align="right" height="320"><fmtProjectDetailsSetup:message key="LBL_DETAILED_DESCRIPTION"/>:&nbsp;</td> 
		                	<td><textarea name="prjDtlDesc" class="HtmlTextArea" onfocus="changeBgColor(this,'#AACCE8');" 
								onblur="changeBgColor(this,'#ffffff');"  rows="3" cols="50"><bean:write name="frmProjectDetails" property="prjDtlDesc"/></textarea> 
							</td>
		                </tr>
		                <tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr>
							<td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;<fmtProjectDetailsSetup:message key="LBL_TYPE"/>:&nbsp;</td>
							<td><gmjsp:dropdown controlName="prjType" SFFormName="frmProjectDetails" SFSeletedValue="prjType"
							SFValue="alPrjTypeList" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
							</td>
						</tr>
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr class="Shade">
							<td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;<fmtProjectDetailsSetup:message key="LBL_DIVISION"/>:&nbsp;</td>
							<td><gmjsp:dropdown controlName="divisionId" SFFormName="frmProjectDetails" SFSeletedValue="divisionId"
							SFValue="alDivisionList" codeId="DIVISION_ID"  codeName="DIVISION_NAME"  defaultValue="[Choose One]"/>
							</td>
						</tr>
					</table>
				</td>
				</tr>
				<tr class="ShadeRightTableCaption">
					<td height="25" colspan="2" ><a href="javascript:fnShowFilters('tabAttribute');" tabindex="-1" style="text-decoration:none; color:black" >
						<IMG id="tabAttributeimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtProjectDetailsSetup:message key="LBL_ATTRIBUTES"/></a>
					</td>
				</tr>
				<tr>
				<td colspan="2">
					<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0" id="tabAttribute" style="display:block">
						<tr><td colspan="4" class="LLine" height="1"></td></tr>
						<tr>
	                		<td class="RightTableCaption" align="right" width="17%" height="110">&nbsp;<fmtProjectDetailsSetup:message key="LBL_DEVICE_CLASSIFICATION"/>:&nbsp;</td> 
	                		<td width="20%"><%=GmCommonControls.getChkBoxGroup("",alDevClass,"Device",hmPrjDevClassMap,"CODENM")%></td>
	                		<td class="RightTableCaption" align="right" width="15%" height="24"><font color="red">*</font>&nbsp;<fmtProjectDetailsSetup:message key="LBL_MARKET_GROUP"/>:&nbsp;</td> 
	                		<td width="30%"><%=GmCommonControls.getChkBoxGroup("",alPrjGrpList,"Group",hmPrjGrpMap,"CODENM")%></td>
	                	</tr>
						<tr><td colspan="4" class="LLine" height="1"></td></tr>
						<tr class="Shade">
	                		<td class="RightTableCaption" align="right" height="110"><font color="red">*</font>&nbsp;<fmtProjectDetailsSetup:message key="LBL_SEGMENT"/>:&nbsp;</td>
	                		<td><%=GmCommonControls.getChkBoxGroup("",alPrjSgmtList,"Segment",hmPrjSgmMap,"CODENM")%></td>
	                		<td class="RightTableCaption" align="right"><font color="red">*</font>&nbsp;<fmtProjectDetailsSetup:message key="LBL_TECHNIQUE"/>:&nbsp;</td> 
	                		<td><%=GmCommonControls.getChkBoxGroup("",alPrjTechList,"Technique",hmPrjTechMap,"CODENM")%></td>
	                	</tr>
                	</table>
				</td>
				</tr>
				<tr class="ShadeRightTableCaption">
					<td height="25" colspan="2" ><a href="javascript:fnShowFilters('tabMisc');" tabindex="-1" style="text-decoration:none; color:black" >
						<IMG id="tabMiscimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtProjectDetailsSetup:message key="LBL_MISCELLANEOUS"/></a>
					</td>
				</tr>
				<tr>
				<td colspan="2">
				   <!-- Added style="display:inline-table" changes in PC-3662-Edge Browser fixes for Quality Module -->
					<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0" id="tabMisc" style="display:inline-table">
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr>
		                	<td class="RightTableCaption" align="right" width="30%" height="24"><font color="red">*</font>&nbsp;<fmtProjectDetailsSetup:message key="LBL_INITIATIED_BY"/>:</td>  
		                		<td width="60%">&nbsp;<gmjsp:dropdown controlName="prjIniBy" SFFormName="frmProjectDetails" SFSeletedValue="prjIniBy"
								SFValue="alPrjIniList" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/></td> 
		                </tr>
						<tr><td colspan="2" class="LLine" height="1"></td></tr>							
						<logic:equal name="frmProjectDetails" property="strEnableApproval" value="false">
							<frame bordercolor="black" frameborder="0">
								<tr class="Shade">
					                <td class="RightTableCaption" align="right" width="30%" height="24"><font color="red">*</font>&nbsp;<fmtProjectDetailsSetup:message key="LBL_APPROVED_BY"/>:</td> 
					                <td width="30%">&nbsp;<gmjsp:dropdown controlName="prjAprvdBy" SFFormName="frmProjectDetails" SFSeletedValue="prjAprvdBy"
											SFValue="alPrjAprvdList" codeId="CODEID"  disabled ="disabled" codeName="CODENM"  defaultValue="[Choose One]"/>
									</td>
								</tr>
								<tr><td colspan="2" class="LLine" height="1"></td></tr>	
							</frame>
						</logic:equal>
						<logic:equal name="frmProjectDetails" property="strEnableApproval" value="true">
							<frame bordercolor="black" frameborder="0" >
								<tr class="Shade">
					                <td class="RightTableCaption" align="right" width="30%" height="24"><font color="red">*</font>&nbsp;<fmtProjectDetailsSetup:message key="LBL_APPROVED_BY"/>:</td> 
					                <td width="30%">&nbsp;<gmjsp:dropdown controlName="prjAprvdBy" SFFormName="frmProjectDetails" SFSeletedValue="prjAprvdBy"
											SFValue="alPrjAprvdList" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
									</td>
								</tr>
								<tr><td colspan="2" class="LLine" height="1"></td></tr>	
							</frame>
						</logic:equal>
						<tr>
	                		<td class="RightTableCaption" align="right" width="30%" height="24"><fmtProjectDetailsSetup:message key="LBL_STATUS"/>:</td> 
	                		<td width="60%">&nbsp;<bean:write name="frmProjectDetails" property="prjStatus"/></td>
	                	</tr>
	                	<tr><td colspan="2" class="LLine" height="1"></td></tr>	
					</table>
				</td>
				</tr>
				<tr>
					<td colspan="2"> 
						<jsp:include page="/common/GmIncludeLog.jsp" >
						<jsp:param name="FORMNAME" value="frmProjectDetails" />
						<jsp:param name="ALNAME" value="alLogReasons" />
						<jsp:param name="LogMode" value="Edit" />
						<jsp:param name="Mandatory" value="yes" />
						</jsp:include>
					</td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
				<tr>
					<td colspan="2" align="center" height="24">&nbsp;
					    <logic:equal name="frmProjectDetails" property="strEnableSubmit" value="false">
                    	<fmtProjectDetailsSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button disabled="true" value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnPrjSubmit(this.form);"/>&nbsp;&nbsp;
                    	</logic:equal>
                    	<logic:equal name="frmProjectDetails" property="strEnableSubmit" value="true">
                    	<fmtProjectDetailsSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button controlId="Btn_Submit"  value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnPrjSubmit(this.form);"/>&nbsp;&nbsp;
                    	</logic:equal>
                    	<logic:equal name="frmProjectDetails" property="strEnableReset" value="false">
                    	<fmtProjectDetailsSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button  disabled="true" value="${varReset}" gmClass="button" buttonType="Save" onClick="fnPrjReset(this.form);"/>&nbsp;&nbsp;
                    	</logic:equal>
                    	<logic:equal name="frmProjectDetails" property="strEnableReset" value="true">
                    	<fmtProjectDetailsSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnPrjReset(this.form);"/>&nbsp;&nbsp;
                    	</logic:equal>
                    	<logic:equal name="frmProjectDetails" property="strEnableChangeStatus" value="false">
                    		<fmtProjectDetailsSetup:message key="BTN_CHANGE_STATUS" var="varChangeStatus"/><gmjsp:button disabled="true" value="${varChangeStatus}" buttonType="Save" gmClass="button" onClick="fnChangePrjStatus(this.form);"/>
                    	</logic:equal>
                    	<logic:equal name="frmProjectDetails" property="strEnableChangeStatus" value="true">
                    		<fmtProjectDetailsSetup:message key="BTN_CHANGE_STATUS" var="varChangeStatus"/><gmjsp:button value="${varChangeStatus}" gmClass="button" buttonType="Save" onClick="fnChangePrjStatus(this.form);"/>
                    	</logic:equal>
					</td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
			</table>
		</td>
	</tr>			
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>