 <%
/*******************************************************************************************************
 * File		 		: GmSubComponentMappingSetup.jsp
 * Desc		 		: This is the JSP that is mentioned for Seu Component Part Mapping Details for France Portal
 * Version	 		: 1.0
 * author			: HReddi
*********************************************************************************************************/
%>

<%@ page language="java"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtSubComponentMappingSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmSubComponentMappingSetup.jsp -->
<fmtSubComponentMappingSetup:setLocale value="<%=strLocale%>"/>
<fmtSubComponentMappingSetup:setBundle basename="properties.labels.prodmgmnt.GmSubComponentMappingSetup"/>

<bean:define id="gridData" name="frmSubComponentMapping" property="strXMLGrid" type="java.lang.String"> </bean:define>
<bean:define id="successMessage" name="frmSubComponentMapping" property="successMessage" type="java.lang.String"> </bean:define>
<bean:define id="pricingAccessFl" name="frmSubComponentMapping" property="pricingAccessFl" type="java.lang.String"> </bean:define>
<bean:define id="qualityAccessFl" name="frmSubComponentMapping" property="qualityAccessFl" type="java.lang.String"> </bean:define>
<bean:define id="strErrorPartMsg" name="frmSubComponentMapping" property="strErrorPartMsg" type="java.lang.String"> </bean:define>
<bean:define id="strErrorSubPartMsg" name="frmSubComponentMapping" property="strErrorSubPartMsg" type="java.lang.String"> </bean:define>
<bean:define id="duplicateSubCompMessage" name="frmSubComponentMapping" property="duplicateSubCompMessage" type="java.lang.String"> </bean:define>

<%
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strWikiTitle = GmCommonClass.getWikiTitle("SUB_COMPONENT_MAPPING_SETUP");
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
//String strSubmitBtnAcces = submitBtnAccess;
String strBtnDisable = "false";
/* if(strSubmitBtnAcces.equals("Y")){
    strBtnDisable = "false";
} */
%>

<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>Globus Medical: Part Price Adjustment Initiate</title>
<style type="text/css">
table.DtTable1050 {
	margin: 0 0 0 0;
	width: 1050px;
	border: 1px solid  #676767 !important;
}
 .even{
			background-color:#ffffff !important;
		}
		.uneven{
			background-color:#dcedfc !important;;
		}
		
		div.gridbox table.hdr td {
   
    color: #000000 !important;
    background-color: #d2e9fc !important;
    }
    div.gridbox_material.gridbox .xhdr {
    background:#dcedfc !important;
}
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmSubComponentMapping.js"></script>

 <script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script> 
<%-- <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
 <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxMenu/dhtmlxmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_validation.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<!-- The library provides support for different keyboard commands that let users to navigate through the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script> --%>

<script>
var objGridData;
objGridData ='<%=gridData%>';
var pricingAccess = '<%=pricingAccessFl%>';
var qualityAccess = '<%=qualityAccessFl%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmSubComponentMapping.do?method=savePartSubCompDtls">
<html:hidden property="strOpt" name="frmSubComponentMapping" />
<html:hidden property="strInputString" name="frmSubComponentMapping" />
<html:hidden property="partInputString" name="frmSubComponentMapping" />
<html:hidden property="screenAccessFl" name="frmSubComponentMapping" />

	<table border="0" class="DtTable1050" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" width="90%">&nbsp;<fmtSubComponentMappingSetup:message key="LBL_SUBPART_MAPPING_SETUP"/></td>
			<td  align="right" class="RightDashBoardHeader" width="10%">
				<fmtSubComponentMappingSetup:message key="IMG_ALT_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:pointer' src='<%=strImagePath%>/help.gif' title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>
			</td>
		</tr>		
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
		<tr height="30">
			<td class="RightTableCaption" align="left" height="30" colspan="2">
				<div id="rfs_grid" style="display:inline" >
					<table cellpadding="1" width="100%" cellspacing="1" border="0">
						<tr>
							<td class="RightTableCaption">
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="copy" style="border: none;" height="14"></a>
								<fmtSubComponentMappingSetup:message key="LBL_ADD_ROWS_FROM_CLIPBOARD" var="varAddRowsClipboard"/>
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" alt="${varAddRowsClipboard}" style="border: none;" onClick="javascript:addRowFromClipboard()" height="14"></a>
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="Add Row" style="border: none;" onClick="javascript:addRow()"height="14"></a>
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="Remove Row" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="paste" style="border: none;" onClick="javascript:pasteToGrid()" height="14"></a>
							</td>									
						</tr>
						<tr>
							<td height="17"><font color="#800000">&nbsp;<fmtSubComponentMappingSetup:message key="LBL_MAXIMUM_ROWS_TO_PASTE"/></font></td>
						</tr>
					</table>
				</div>
			</td>									
		</tr>		
		<tr><td colspan="2" ><div id="dataGridDiv"  style="height:450px;"></div></td></tr>		
		<tr height="30" class="Shade">
			<td  class="RightTableCaption" colspan="9" align="center">	
			<fmtSubComponentMappingSetup:message key="BTN_SUBMIT" var="varSubmit"/>	
			&nbsp;<gmjsp:button name="Btn_Save" value="${varSubmit}" gmClass="button" style="width:5em;cursor:pointer" onClick="javascript:fnSubmitAction();" disabled="<%=strBtnDisable %>" buttonType="Load" />
			</td>
		</tr>		
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
		<logic:notEmpty name="frmSubComponentMapping" property="successMessage">					
		<tr><td colspan="8" align="left" class="RightTableCaption">
			<ul> <li type="square"><font color="green"><bean:write name="frmSubComponentMapping" property="successMessage"></bean:write></font> </li> </ul>
		</td></tr>
		</logic:notEmpty>
		<logic:notEmpty name="frmSubComponentMapping" property="headerMessage">
		<tr><td colspan="8" align="left" class="RightTableCaption">
			<ul> <li type="square"><font color="red"><bean:write name="frmSubComponentMapping" property="headerMessage"></bean:write></font></li> </ul>
		</td></tr>
		</logic:notEmpty>
		<logic:notEmpty name="frmSubComponentMapping" property="strErrorPartMsg">
		<tr><td colspan="8" align="left" class="RightTableCaption">
			<ul> <li type="square"><font color="red"><%=strErrorPartMsg %></font></li> </ul>
		</td></tr>
		</logic:notEmpty>
		<logic:notEmpty name="frmSubComponentMapping" property="strErrorSubPartMsg">
		<tr><td colspan="8" align="left" class="RightTableCaption">
			<ul> <li type="square"><font color="red"><%=strErrorSubPartMsg %></font></li> </ul>
		</td></tr>
		</logic:notEmpty>
		<logic:notEmpty name="frmSubComponentMapping" property="duplicateSubCompMessage">
		<tr><td colspan="8" align="left" class="RightTableCaption">
			<ul> <li type="square"><font color="red"><%=duplicateSubCompMessage %></font></li> </ul>
		</td></tr>
		</logic:notEmpty>	
	</table>
<%@ include file="/common/GmFooter.inc"%>
</html:form>
</body>
</html>