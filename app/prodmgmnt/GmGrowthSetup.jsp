<%
/**********************************************************************************
 * File		 		: GmGrowthSetup.jsp
 * Desc		 		: Setting up Growth
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtGrowthSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtGrowthSetup:setLocale value="<%=strLocale%>"/>
<fmtGrowthSetup:setBundle basename="properties.labels.prodmgmnt.GmGrowthSetup"/>

<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.beans.GmGridFormat"%>
<%@ page import ="java.util.HashMap"%>
<%@ page import ="java.util.ArrayList"%>
<bean:define id="strRefType" name="frmGrowthSetup" property="refType" type="java.lang.String"> </bean:define>

<bean:define id="prjStatus" name="frmGrowthSetup" property="prjStatus" type="java.lang.String"> </bean:define>
<bean:define id="launchDt" name="frmGrowthSetup" property="launchDt" type="java.lang.String"> </bean:define>
<bean:define id="validateMonth" name="frmGrowthSetup" property="validateMonth" type="java.lang.Integer"> </bean:define>
<bean:define id="validateYear" name="frmGrowthSetup" property="validateYear" type="java.lang.Integer"> </bean:define>
<bean:define id="alRefIds" name="frmGrowthSetup" property="alRefIds" type="java.util.ArrayList"> </bean:define>
<bean:define id="refType" name="frmGrowthSetup" property="refType" type="java.lang.String"> </bean:define>
<bean:define id="hmGrowthData" name="frmGrowthSetup" property="growthDetailsMap" type="java.util.HashMap"> </bean:define>
<bean:define id="strDemandSheetId" name="frmGrowthSetup"  property="demandSheetId" type="java.lang.String"></bean:define>
<bean:define id="strDemandSheetName" name="frmGrowthSetup"  property="searchdemandSheetId" type="java.lang.String"></bean:define> 

<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("OP_GROWTH_SETUP");
    GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.prodmgmnt.GmGrowthSetup", strSessCompanyLocale);
	String disabledenabled = "";	
	GmGridFormat gmGridFormat = new GmGridFormat();
	//HashMap hmGrowthData = (HashMap)request.getAttribute("GROWTH_DATA");
	gmGridFormat.setHideColumn("Total");	
	String strGridData = "";
	String strDivHeight = "150";
	String strDivWidth = "870";
	int listSize=0;
	boolean showGrid=false;
	if(hmGrowthData!=null && hmGrowthData.size()>0){
		
		if(hmGrowthData.get("Header")!=null && ((ArrayList)hmGrowthData.get("Header")).size()>0){
			showGrid = true;			
		}
		if(hmGrowthData.get("Details")!=null)
			listSize = ((ArrayList)hmGrowthData.get("Details")).size();
		
		if(listSize >0 && listSize <=5){
			strDivHeight="150";		
		}else if(listSize >=6 && listSize <=15){
			strDivHeight="200";		
		}else if(listSize >=16 && listSize <=25){
			strDivHeight="225";		
		}else if(listSize >=26 && listSize <=50){
			strDivHeight="250";		
		}else if(listSize >=51 && listSize <=100){
			strDivHeight="275";
		}else if(listSize >=101 && listSize <=125){
			strDivHeight="600";
		}
		
		gmGridFormat.setDecorator("com.globus.crosstab.beans.GmGrowthGridDecorator");
		
		//gmGridFormat.reNameColumn("Name","RAJ");
		gmGridFormat.setColMasterList("Name",alRefIds);
		gmGridFormat.setColMasterKey("Name","REFID","REFNM","false");
		gmGridFormat.setColumnWidth("Name",150);
		gmGridFormat.setColumnWidth("Description",150);
		gmGridFormat.addColumnSortType("Name","str");
		gmGridFormat.addColumnAlign("Name","left");
		gmGridFormat.addColumnSortType("Description","str");
		gmGridFormat.addColumnAlign("Description","left");
		gmGridFormat.addColumnType("Description","ro");
		// If the Ref Type is Group ID on Sales Sheet.
		if(refType!=null && !refType.equals("20297")){
			gmGridFormat.setAddColumn(GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_Description")),1); // Adding the column to 2nd postion.
			strDivWidth = "950";
			gmGridFormat.setColumnWidth("Name",80);
			gmGridFormat.setColMasterKey("Name","REFID","REFNM","true");
		}	
		strGridData = gmGridFormat.generateGridXML(hmGrowthData);
	}
	
	String strNameDropDownValues = gmGridFormat.getDropDownMasterValue("Name");
	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Growth Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxMenu/skins/dhtmlxmenu_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmGrowthSetup.js"></script>


<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxMenu/dhtmlxmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_undo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<script>
var year = '<%=validateYear%>';
var month = '<%=validateMonth%>';
var prjSt = '<%=prjStatus%>';
var launchdt = '<%=launchDt%>';
var varNameDropDownValues = '<%=strNameDropDownValues%>';
var hFromDate = '<%=request.getAttribute("hidden_FromDate")%>';
var hToDate = '<%=request.getAttribute("hidden_ToDate")%>';

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad()">
<html:form action="/gmGrowthSetup.do"  >
<html:hidden  property="hAction" value="" />
<html:hidden property="strOpt" value=""/>
<html:hidden property="growthId" />
<html:hidden property="hModifiedRowID" value="" />
<html:hidden property="msg" />
<html:hidden property="inputString" />
<html:hidden property="overrideTypeId" />
<html:hidden property="refIdForLogReason"/>
<input type="hidden" name="hCancelType" value="VDREQT">
<input type="hidden" name="hTxnId" value="">

<table border="0" width="970" cellspacing="0" cellpadding="0">
	<tr>
		<td width="1" class="Line"></td>
		<td height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr><td class="Line" colspan="3"></td></tr>
				<tr><td colspan="2" height="25" class="RightDashBoardHeader"><fmtGrowthSetup:message key="LBL_GROWTH_SETUP"/></td>
				<td align="right" class=RightDashBoardHeader > 	
				<fmtGrowthSetup:message key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
				</tr>
				<logic:notEqual name="frmGrowthSetup" property="msg" value="">
				<tr><td colspan="2" height="20" align="center" class="RightBlueText"><bean:write name="frmGrowthSetup" property="msg"/></td></tr>
				<tr><td class="lline" colspan="3"></td></tr>
				</logic:notEqual>
				<tr height="25" class="Shade">
					<td class="RightTableCaption" nowrap align="right" HEIGHT="24"><fmtGrowthSetup:message key="LBL_DEMAND_SHEET"/>:</td>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<td colspan="2"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="demandSheetId" />
					<jsp:param name="METHOD_LOAD" value="loadDemandSheetList&searchType=PrefixSearch" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="1"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strDemandSheetName%>" />
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strDemandSheetId%>" />
					<jsp:param name="SHOW_DATA" value="100" />
					<jsp:param name="AUTO_RELOAD" value="javascript:fnReloadReferences();"  />
			     	</jsp:include>
						</td>
					</tr>
				 <tr><td colspan="3" class="lline"></td></tr>
				<%if( !strRefType.equals("20295") && !strRefType.equals("20298")){
								 	log.debug("The value...."+strRefType);
							 		disabledenabled = "true";
							 	}%>
				<tr>
				<td  align="right" HEIGHT="24" class="RightTableCaption"><font color="red">*</font> <fmtGrowthSetup:message key="LBL_REF_TYPE"/>:</td>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<td>&nbsp;<gmjsp:dropdown controlName="refType" SFFormName="frmGrowthSetup" SFSeletedValue="refType" tabIndex="2"
							SFValue="alRefTypes" onChange="javascript:fnReload();" width="150" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
							&nbsp;&nbsp;<fmtGrowthSetup:message key="BTN_VIEW" var="varView"/><gmjsp:button value="${varView}" tabindex="3" gmClass="button" buttonType="Load" onClick="javascript:fnOpenValidate();" />&nbsp;&nbsp;&nbsp;
							<fmtGrowthSetup:message key="BTN_VOID_PART" var="varVoidPart"/><gmjsp:button  disabled="<%=disabledenabled%>" name="voiding" 
							value="${varVoidPart}" tabindex="3" style="width:100px" gmClass="button" buttonType="Save" onClick="javascript:fnVoid();" />   
							&nbsp;&nbsp;&nbsp;&nbsp;
				</td></tr>
				<tr><td class="lline" colspan="3"></td></tr>
			
			 <tr class="Shade">
				<td class="RightTableCaption" nowrap colspan="1" align="right" HEIGHT="24"><font color="red">*</font> <fmtGrowthSetup:message key="LBL_FRM_DATE"/>:</td>
				<td class="RightTableCaption" align="left" colspan="2" height="20">
					<table cellpadding="1" cellspacing="1" border="0" width="650" >
						<TR class="Shade">
								<td class="RightTableCaption">&nbsp;
								<html:text styleId="fromDate" property="fromDate" size="10" maxlength="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="4"/>
								&nbsp;&nbsp;<!-- <img src="<%=strImagePath%>/nav_calendar.gif" style="cursor:hand"  tabindex="5" onclick="javascript:show_calendar('frmGrowthSetup.fromDate','','','MON/YY');" height="14">-->				
								&nbsp;<font color="red">*</font><fmtGrowthSetup:message key="LBL_TO_DATE"/>:&nbsp;
								<html:text styleId="toDate" property="toDate" size="10" maxlength="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="6"/>
								&nbsp;&nbsp;<img src="<%=strImagePath%>/nav_calendar.gif"  tabindex="7" style="cursor:hand" onclick="javascript:showHideCalendar(hFromDate,hToDate)"  height="14">
								&nbsp;	<fmtGrowthSetup:message key="LBL_PART"/>:
								<html:text property="pnum" size="35" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="8"/>
						&nbsp;&nbsp;<fmtGrowthSetup:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" tabindex="9" gmClass="button" buttonType="Load" onClick="javascript:fnReloadPnum();" />
								</td>										
							</TR>
							<tr>
							<td colspan="3"><div id="dhtmlxDblCalendar" style="position:absolute"></div></td>
							</tr>
				
					</table>
				</td>				
			 </tr>	
			 			
				 <tr><td colspan="3" class="lline"></td></tr>
			
				<%if(showGrid){ %>
				<tr>
				<td class="RightTableCaption" align="left" colspan="3" height="20">
				<div id="growth_data" style="display:inline" >
					<table cellpadding="1" cellspacing="1" border="0" width="470" >
						<TR >
								<td class="RightTableCaption">
								&nbsp;<a href="#"><fmtGrowthSetup:message key="IMG_COPY" var="varCopy"/><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="${varCopy}" style="border: none;" height="14"></a>	
								&nbsp;<a href="#"><fmtGrowthSetup:message key="IMG_PASTE" var="varPaste"/> <img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="${varPaste}" style="border: none;" onClick="javascript:pasteToGrid()" height="14"></a>	
								&nbsp;<a href="#"><fmtGrowthSetup:message key="IMG_REDO" var="varRedo"/> <img src="<%=strImagePath%>/dhtmlxGrid/redo.gif" alt="${varRedo}" onClick="javascript:doredo();" style="border: none;" height="14"></a>
								&nbsp;<a href="#"><fmtGrowthSetup:message key="IMG_UNDO" var="varUndo"/> <img src="<%=strImagePath%>/dhtmlxGrid/undo.gif" alt="${varUndo}" onClick="javascript:doundo();" style="border: none;" height="14"></a>	
								&nbsp;<a href="#"><fmtGrowthSetup:message key="IMG_ADD_ROW" var="varAddRow"/> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="javascript:addRow()"height="14"></a>
								&nbsp;<a href="#"><fmtGrowthSetup:message key="IMG_ADD_ROW_CLIP" var="varAdRoClip"/> <img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" alt="${varAdRoClip}" style="border: none;" onClick="javascript:addRowFromClipboard()" height="14"></a>
								&nbsp;<a href="#"><fmtGrowthSetup:message key="IMG_RMV_ROW" var="varRmRow"/> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRmRow}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
								&nbsp;<a href="#"><fmtGrowthSetup:message key="IMG_UN_RMV_ROW" var="varUnRmRow"/> <img src="<%=strImagePath%>/dhtmlxGrid/undo_delete.gif" alt="${varUnRmRow}" style="border: none;" onClick="javascript:enableSelectedRow()" height="14"></a>
								&nbsp;<a href="#"><fmtGrowthSetup:message key="IMG_EN_MARK" var="varEnMark"/> <img src="<%=strImagePath%>/dhtmlxGrid/highlight.gif" alt="${varEnMark}" style="border: none;" onClick="javascript:enableMarker()"  height="14"></a>
								&nbsp;<a href="#"><fmtGrowthSetup:message key="IMG_DS_MARK" var="varDsMark"/> <img src="<%=strImagePath%>/dhtmlxGrid/disable-highlight.gif" alt="${varDsMark}" style="border: none;" onClick="javascript:disableMarker()" height="14"></a>
								&nbsp;<html:text property="applyToMarked" size="4" maxlength="3"  onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="9"/>
									&nbsp;&nbsp;<fmtGrowthSetup:message key="BUT_APP_MARK" var="varAppMark"/> <gmjsp:button value="${varAppMark}" tabindex="10" gmClass="button" buttonType="Save" onClick="javascript:applyToMarkedCell();" />
								</td>										
							</TR>
				
					</table>
				</div>
				</td>				
			 </tr>
			 	<tr>
		<td colspan="3">
			<div id="mygrid_container" style="width:<%=strDivWidth%>px;height:<%=strDivHeight%>px"></div>			
		</td>
	</tr>
	<script>
	<% 
	long startTime = System.currentTimeMillis();
	log.debug("Grid Start Time"+startTime); 
	%>
	  var gridData= '<%=strGridData%>';
	  if(gridData.length>0){ 
	  
		initializeGrid();
	}
	

	</script>
	<%
	log.debug("End Time"+System.currentTimeMillis() +"  Time Taken : "+(System.currentTimeMillis()-startTime));
	 %>
	
					<td colspan="2" align="center" height="40">&nbsp;
					<fmtGrowthSetup:message key="BUT_Submit" var="varSubmit"/><gmjsp:button value="${varSubmit}" name="submitbtn" tabindex="11" gmClass="button" buttonType="Save" onClick="javascript:fnSubmit();" />&nbsp;
					<fmtGrowthSetup:message key="BUT_Reload" var="varReload"/><gmjsp:button value="${varReload}" tabindex="12" gmClass="button" buttonType="Load" onClick="javascript:fnReloadPnum()" />
					</td>
				</tr>
				<%} %>
			</table>
		</td>
		<td width="1" class="Line"></td>
	</tr>
	<tr><td height="1" colspan="3" class="Line"></td></tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
