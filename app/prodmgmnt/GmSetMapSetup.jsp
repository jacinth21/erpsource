 <%
/**********************************************************************************
 * File		 		: GmSetMapSetup.jsp
 * Desc		 		: This screen is used for the Set and Part Number Mapping
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib prefix="fmtSetMapSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmSetMapSetup.jsp -->
<fmtSetMapSetup:setLocale value="<%=strLocale%>"/>
<fmtSetMapSetup:setBundle basename="properties.labels.prodmgmnt.GmSetMapSetup"/>
<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strSessDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
 	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strhOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
  	String strUserStatusFlag = GmCommonClass.parseNull((String)request.getAttribute("strUserStatusFlag"));
  	String strJSONString = "{\"STROPT\":\""+strhOpt+"\"}";
	String strSetName = "";
	String strSetId =	"";
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strCheckedCrit = "";
	String strPartNum = "";
	String strInSetFl = "";
	String strCritFl = "";
	String strDesc = "";
	String strQty = "";
	String strSelectedIds = "";
	String strId = "";
	String strCriticalQty = "";
	String strCriticalTag = "";
	String strCriticalType = "";
	String strUnitType = "";
	String strDisabled = "disabled";
	String strCriticalQtyFl = "disabled";
	String strSetTypeFL = ""; 
	String  strPartFamilyFL="", strProdAttributeValue="";
	String strAddrowFL = GmCommonClass.parseNull((String)request.getAttribute("ADDROWACCFL"));
	String strAccessFL = GmCommonClass.parseNull((String)request.getAttribute("ACCESSFL"));
	String strWikiTitle = "";
	String strWidth = "470";
	String strBOMButtonDisableFl = "";
	
	ArrayList alSetList = new ArrayList();
	ArrayList alSetPartMapList = new ArrayList();
	ArrayList alCritType = new ArrayList();
	ArrayList alUnitType = new ArrayList();
	
	strWikiTitle = GmCommonClass.getWikiTitle("SET_MAPPING");
	if(strhOpt.equals("BOMMAP"))
	{
	strWikiTitle = GmCommonClass.getWikiTitle("BOM_MAPPING");
	strWidth = "350";
	}
	
	if (hmReturn != null)
	{
		alSetList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETMASTERLIST"));
		// PMT-50777: to disable the button based on flag (BOM mapping)
		strBOMButtonDisableFl = GmCommonClass.parseNull((String) hmReturn.get("BOM_BUTTON_DISABLE_FL"));
		
		if (strhAction.equals("Go"))
		{
			alSetPartMapList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETMAPPEDLIST"));
			strSetName = GmCommonClass.parseNull((String)request.getAttribute("hSetName"));
			if(strSetName.contains("\"")){
			  strSetName = strSetName.replaceAll("\"","&quot;");
			}
			strSetId = GmCommonClass.parseNull((String)request.getAttribute("hSetId"));
			alCritType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETCRITTYPE"));
			alUnitType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETUNITTYPE"));
		}
	}

	int intMapSize = alSetPartMapList.size();
	int intSize = 0;
	HashMap hcboVal = null;
	if (strSessDeptId.equals("Z")|| strUserStatusFlag.equals("Y"))
	{
		strDisabled = "";
	}
	if (strAccessFL.equals("Y"))
	{
		strCriticalQtyFl = "";
	}
	String checkCtriFl = ""; 
	String checkTaggableFl = ""; 
	
	for (int i=0;i<intMapSize;i++)
	{
			hcboVal = (HashMap)alSetPartMapList.get(i);
			 
			strCritFl = GmCommonClass.parseNull((String)hcboVal.get("CRITICALFL")); 
			
			// If Set type = "Sales/Consignment" and no critical Flag checked ,
			strSetTypeFL = GmCommonClass.parseNull((String)hcboVal.get("SETTYPEFL"));  //Sales or consignment 
		 
			 if(strSetTypeFL.equals("Y")&&strCritFl.equals("Y"))
			 {	
			  	checkCtriFl = "Y";
			 }
			// If Part number family = Graphical Case and Taggable =Y, 
			strPartFamilyFL = GmCommonClass.parseNull((String)hcboVal.get("PARTFAMILYFL")); // Graphical Case 
			strProdAttributeValue = GmCommonClass.parseNull((String)hcboVal.get("PRODATTRIBUTEVALUE")); // Taggable Part 4052
			
			 if( strPartFamilyFL.equals("Y") && strProdAttributeValue.equals("Y"))
			 {	
			  	checkTaggableFl = "Y";
			 }
			
		}
	String strMandatory = GmCommonClass.parseNull(request.getParameter("Mandatory"));
%>
<HTML>
<jsp:useBean id="brandBean" type="com.globus.common.beans.GmResourceBundleBean" class="com.globus.common.beans.GmResourceBundleBean" scope="request"> </jsp:useBean>
<TITLE><%=brandBean.getProperty("Title")%></TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmSetMapSetup.js"></script>

<script>
var flaghA = false;
var strType = '<%=strhOpt%>';
var disabled = "<%=strDisabled%>";
var count = '<%=intMapSize%>';
var strUserStatusFlag ='<%=strUserStatusFlag%>';
var bomButtonDisableFl = '<%=strBOMButtonDisableFl%>';

function fnVoidSetPart(partNum, partDesc)
{
	var hOptVal = document.frmVendor.hOpt.value;
	var setnm = encodeURIComponent(TRIM(document.frmVendor.hSetName.value));
	var setName = message_prodmgmnt[340];
	if(hOptVal == 'BOMMAP'){
		setName = message_prodmgmnt[341];
	}
	var setIdVal = document.frmVendor.Cbo_Set.value;
	document.frmVendor.hAction.value = "Load";
	document.frmVendor.hTxnId.value = partNum;
	document.frmVendor.hTxnName.value = partDesc;
	document.frmVendor.hRedirectURL.value =  "/GmSetMapServlet?hAction=Go&hOpt="+hOptVal+"&Cbo_Set="+setIdVal+"&searchCbo_Set="+setnm;
	document.frmVendor.hDisplayNm.value = setName;
	document.frmVendor.action ="<%=strServletPath%>/GmCommonCancelServlet";
	document.frmVendor.submit();
}

 
 
</SCRIPT>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnSetSelections();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmSetMapServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<INPUT type="hidden" name="inputString" value="">
<input type="hidden" name="hOpt" value="<%=strhOpt%>">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hTxnName" value="">
<input type="hidden" name="hCancelType" value="VSPMP">
<input type="hidden" name="hRedirectURL" value="">
<input type="hidden" name="hDisplayNm" value="">
<input type="hidden" name="hSetId" value ="<%=strSetId%>">
<input type="hidden" name="hSetName" value ="<%=strSetName%>">
<!-- Custom tag lib code modified for JBOSS migration changes -->

	<table class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="5" class="RightDashBoardHeader"><%=brandBean.getProperty("Title")%></td>
			<td class="RightDashBoardHeader" align="right"> 
			<fmtSetMapSetup:message key="IMG_ALT_HELP" var="varHelp"/>
			<img align="middle" id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
			<td align="right" class="RightTableCaption" colspan="1" width="15%" HEIGHT="40"><%=brandBean.getProperty("CaptionName")%>:&nbsp;</td>
		<td align ="left" colspan="1" style ="width: 0px;"><input type="text" size="15" value="<%=strSetId%>" name="Txt_SetId" id="Txt_SetId" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');javascript:fnGO('id');" tabindex=1>
						</td>
			<td colspan="3" aligh = "left"><table><tr><td><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="Cbo_Set" />
					<jsp:param name="PARAMETER" value="<%= strJSONString%>" />
					<jsp:param name="METHOD_LOAD" value="loadSetNameList&searchType=PrefixSearch" />
					<jsp:param name="WIDTH" value="550" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="2"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%= strSetName%>" />
					<jsp:param name="CONTROL_ID_VALUE" value="<%= strSetId%>" />
					<jsp:param name="SHOW_DATA" value="10" />
					<jsp:param name="AUTO_RELOAD" value="fnGO('name');" />

				</jsp:include></Td></tr></table>
						</td>
								
				<%-- <fmtSetMapSetup:message key="BTN_LOAD" var="varLoad"/>
			<td colspan="2" align="center"><gmjsp:button gmClass="Button" name="Btn_Go" value="&nbsp;${varLoad}&nbsp;" buttonType="Load" onClick ="fnGo();" /></td> --%>
		</tr>
					
<%
					if (strhAction.equals("Go"))
					{
%>
			<tr >
					<td height="100" valign="top" colspan="6">
					<table border="0" cellspacing="0" cellpadding="0">
					<tr><td class="Line" height="1"></td></tr>
					<tr>
						<td colspan="2" align="center">
						<div style="overflow:auto; height:350px;">
							<TABLE cellpadding="1" cellspacing="1" border="0" width="99%" bgcolor="#666666"  id="PriceTable">
							<tr><td bgcolor="#eeeeee" colspan="9" height="1"></td></tr>
								<thead>
									<tr class="Shade" style="position:relative; top:expression(this.offsetParent.scrollTop);">
										<th nowrap class="RightText" height="35" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_PART_NUMBER"/></th>
										<th nowrap class="RightText" width="<%=strWidth%>">&nbsp;<fmtSetMapSetup:message key="LBL_DESCRIPTION"/></th>
										<th nowrap class="RightText" align="center">&nbsp;<%=brandBean.getProperty("CaptionQty")%></th>
										<th nowrap class="RightText" align="center" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_SET_FLAG"/><BR></th>
										<th nowrap class="RightText" align="center" width="60" id="strCrit_Flag" >&nbsp;<fmtSetMapSetup:message key="LBL_CRITICAL_FLAG"/><BR></th>
										<th nowrap class="RightText" align="center" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_CRITICAL_QTY"/><BR></th>
										<th nowrap class="RightText" align="center" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_CRITICAL_TYPE"/><BR></th>
										
										<%  if (strhOpt.equals("BOMMAP")) {%>
										<th nowrap class="RightText" align="center" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_TAG"/></th>
										<th nowrap class="RightText" align="center" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_UNIT_OF_MEASUREMENT"/><br></th>
										<%
									  		}else {
										%>	
										
										<th nowrap class="RightText" align="center" width="60">&nbsp;<fmtSetMapSetup:message key="LBL_CRITICAL_TAG"/><BR></th>
										
										<%
									  		} 
										%>
									</TR>
								</thead>
								<tbody>
<%
					String strTemp = "";
					String strUnit = "";
					hcboVal = new HashMap();
					int count=0;
					for (int i=0;i<intMapSize;i++)
			  		{
			  			
			  			hcboVal = (HashMap)alSetPartMapList.get(i);
			  			strId = (String)hcboVal.get("MID");
			  			strPartNum = (String)hcboVal.get("PNUM");
						strDesc = GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"));
						//To avoid single,double quotes while passing into function
						if(strDesc.contains("\'")){
							strDesc = strDesc.replaceAll("\'", "&rsquo; ");
						} 
						if(strDesc.contains("\"")){
							strDesc = strDesc.replaceAll("\"","&QUOT;");
						}
						//strDesc = GmCommonClass.formatSpecialCharFromDB(strDesc); code commented for bug-5387 fix
						strQty = GmCommonClass.parseZero((String)hcboVal.get("QTY"));
						strInSetFl = GmCommonClass.parseNull((String)hcboVal.get("INSETFL"));
						strCritFl = GmCommonClass.parseNull((String)hcboVal.get("CRITICALFL"));
						strChecked = strInSetFl.equals("Y")?"checked":"";
						strCheckedCrit = strCritFl.equals("Y")?"checked":"";
						strCriticalQty = GmCommonClass.parseNull((String)hcboVal.get("CRITQTY"));
						strCriticalQty = strCriticalQty.equals("") && strCritFl.equals("Y")?strQty:strCriticalQty;
						strCriticalTag = GmCommonClass.parseNull((String)hcboVal.get("CRITTAG"));
						strCriticalType = GmCommonClass.parseNull((String)hcboVal.get("CRITTYPE"));
						strUnitType = GmCommonClass.parseNull((String)hcboVal.get("UNITTYPE"));
					 
						strProdAttributeValue = GmCommonClass.parseNull((String)hcboVal.get("PRODATTRIBUTEVALUE")); // Taggable Part 4052, Graphical Case
						
						
						strTemp = "Cbo_CritType"+i;
						strUnit = "Cbo_UnitType"+i;
%>
									<tr bgcolor="#ffffff" onMouseOver="changeTRBgColor(<%=i%>,'#EEEEEE');" id="tr<%=i%>">
										<fmtSetMapSetup:message key="LBL_CLICK_TO_VOID" var="varClickToVoid"/>
									<%  if (strhOpt.equals("BOMMAP")) {%>
										<td class="RightText" HEIGHT="20">&nbsp;<%=strPartNum%><input type="hidden" name="Txt_PNum<%=i%>" value="<%=strPartNum%>">
										<input type="hidden" name="hId<%=i%>" value="<%=strId%>"></td>
									<%
												}else{
													
									%>
										<td class="RightText" HEIGHT="20">&nbsp; <a href="javascript:fnVoidSetPart('<%=strId%>', '<%=strDesc%>');" title="${varClickToVoid}"> 
										<img src='<%= strImagePath%>/void.jpg' border='0' height='14' width='14' /></a>&nbsp;<%=strPartNum%><input type="hidden" name="Txt_PNum<%=i%>" value="<%=strPartNum%>">
										<input type="hidden" name="hId<%=i%>" value="<%=strId%>"></td>
									<%
												}
													
									%>			
									
										<td class="RightText">&nbsp;<%=strDesc%></td>
										<td class="RightText" align="center"><input type="text" size="2" class="InputArea" value="<%=strQty%>" name="Txt_Qty<%=i%>"></td>
										<td class="RightText" align="center"><input type="checkbox" <%=strChecked%> name="Chk_Set<%=i%>"></td>
										<td class="RightText" align="center"><input type="checkbox" <%=strCheckedCrit%> <%=strDisabled%> name="Chk_Crit<%=i%>" id="Chk_Crit<%=i%>"  ></td>
										<% if (strCountryCode.equals("en")){%>
										<td class="RightText" align="center"><input type="text" size="2" class="InputArea" <%=strDisabled%> value="<%=strCriticalQty%>" name="Txt_CritQty<%=i%>"></td>
										<%}else {%>
										<td class="RightText" align="center"><input type="text" size="2" class="InputArea" <%=strCriticalQtyFl%> value="<%=strCriticalQty%>" name="Txt_CritQty<%=i%>"></td>
										<%} %>

										<td class="RightText" align="center"><gmjsp:dropdown controlName= "<%=strTemp%>" disabled="<%=strDisabled%>" seletedValue="<%=strCriticalType%>" 	
							   			value="<%=alCritType%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"  /></td>
										<td class="RightText" align="center"><input type="text" size="2" class="InputArea" <%=strDisabled%> value="<%=strCriticalTag%>" name="Txt_CritTag<%=i%>"></td>
										
										 
										<input type="hidden" name="strProdAttributeValue<%=i%>" id="strProdAttributeValue<%=i%>" value="<%=strProdAttributeValue%>" />
										
										
									<%  if (strhOpt.equals("BOMMAP")) {%>
					
										<td class="RightText" align="center"><gmjsp:dropdown controlName= "<%=strUnit%>" disabled="<%=strDisabled%>" seletedValue="<%=strUnitType%>" 	
							   												value="<%=alUnitType%>" codeId = "CODEID"  codeName = "CODENM"   /></td>
									<%
												}
									%>								
									</tr>
									
<%
			  		}
%>								
								</tbody>
							</table>
						</div>
						</td>
				   </tr>
				   <tr><td>
				   <%  		if (!strhOpt.equals("BOMMAP")) 
				   				{ if (!checkCtriFl.equals("Y")) { 
								 %>
								<div id="strCritical_Qty"><strong><font color="red"><center><fmtSetMapSetup:message key="LBL_NO_PART_MARKED"/></center></font></strong></div>
								<%
									}	
			   					if ((!checkTaggableFl.equals("Y")) && (strCountryCode.equals("en"))) {  
								%>
								<div id="strTaggable_part"><strong><font color="red"><center><fmtSetMapSetup:message key="LBL_NO_TAGGABLE_PART"/></center></font></strong></div>
							<%
									}
								}
							%>
					</td></tr>
				<tr>
				<td colspan="2" align="center" height="25">&nbsp;
                <jsp:include page="/common/GmIncludeLog.jsp" >
                	<jsp:param value="yes" name="Mandatory"/> 
                	<jsp:param name="FORMNAME" value="" />
					<jsp:param name="ALNAME" value="" />
					<jsp:param name="LogMode" value="Edit" />																					
				</jsp:include>	
				</td>
			</tr>	
				 
				   <tr>
						<td colspan="2" align="center" height="35">
							<INPUT type="hidden" name="hCnt" value="<%=intMapSize%>">
							<input type="hidden" name="hSelectedIds" value="<%=strSelectedIds%>">
							
						  <% if (!strBOMButtonDisableFl.equals("Y")) {%>  <!-- BOM - Part Number Mapping screen - BOMMAP used to hide AddRow and Submit Button used PMT-50777 -->
							 <% if (strCountryCode.equals("en")){%>
							<fmtSetMapSetup:message key="BTN_ADD_ROW" var="varAddRow"/>
							<gmjsp:button value="${varAddRow}" gmClass="button" buttonType="Save" onClick="fnAddRow('PriceTable');" tabindex="16"/>&nbsp;&nbsp;
							<%}else if(strAddrowFL.equals("Y")){ %>
							<fmtSetMapSetup:message key="BTN_ADD_ROW" var="varAddRow"/>
							<gmjsp:button value="${varAddRow}" gmClass="button" buttonType="Save" onClick="fnAddRow('PriceTable');" tabindex="16"/>&nbsp;&nbsp;
							<%}%>
							<fmtSetMapSetup:message key="BTN_SUBMIT" var="varSubmit"/>
							<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" name="btn_submit" buttonType="Save" onClick="fnSubmit();" tabindex="16"/>&nbsp;  
					       <%
							}
						   %>	
						</td>
					</tr>
<%
					}
%>
				</table>
			</td>
		</tr>
		
    </table>
<div style="visibility:hidden" id="cbohtml">
<gmjsp:dropdown controlName="Cbo_CritType" value="<%=alCritType%>" disabled="<%=strDisabled%>" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
</div>

<%  if (strhOpt.equals("BOMMAP")) {%>
<div style="visibility:hidden" id="cbouhtml">
<gmjsp:dropdown controlName= "Cbo_UnitType" disabled="<%=strDisabled%>"   value="<%=alUnitType%>" codeId = "CODEID"  codeName = "CODENM"   />
</div>
<%
	}
%>

</FORM>
<%@ include file="/common/GmFooter.inc"%>

</BODY>

</HTML>
