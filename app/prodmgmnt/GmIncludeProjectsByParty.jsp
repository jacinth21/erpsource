<%
/**********************************************************************************************
 * File		 		: GmIncludeProjectsByParty.jsp
 * Desc		 		: JSP file to display the projects for party
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
/***********************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtIncludeProjectsByParty" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--\prodmgmnt\GmIncludeProjectsByParty.jsp  -->
<fmtIncludeProjectsByParty:setLocale value="<%=strLocale%>"/>
<fmtIncludeProjectsByParty:setBundle basename="properties.labels.prodmgmnt.GmIncludeProjectsByParty"/>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<display:table name="requestScope.frmStaffingDetails.lresult" requestURI="/gmPDStaffingDetail.do" excludedParams="haction" class="gmits" id="currentRowObject"  
	decorator="com.globus.prodmgmnt.displaytag.DTStaffingCountWrapper">
	<fmtIncludeProjectsByParty:message key="LBL_PROJECT" var="varProject"/><display:column property="PROJECT" title="${varProject}" class="alignleft" />
    <fmtIncludeProjectsByParty:message key="LBL_ROLE" var="varRole"/><display:column property="ROLE" title="${varRole}" class="alignleft" />			            
    <fmtIncludeProjectsByParty:message key="LBL_STATUS" var="varStatus"/><display:column property="STATUS" title="${varStatus}" class="alignleft" /> 
</display:table>