 <%
/**********************************************************************************
 * File		 		: GmPendingOrderReport.jsp
 * Desc		 		: This screen is used for the Account Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib uri="com.corda.taglib" prefix="ctl" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPORpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPORpt:setLocale value="<%=strLocale%>"/>
<fmtPORpt:setBundle basename="properties.labels.custservice.GmPendingOrderReport"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmPendingOrderReport", strSessCompanyLocale);
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmParam = (HashMap)request.getAttribute("hmParam");
	String strWikiTitle = GmCommonClass.getWikiTitle("ORDERS_PENDING_PO");
	ArrayList alReturn =  GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("REPORT"));
	ArrayList alDTypes =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("DTYPE"));
	ArrayList alOTypes =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("OTYPE"));
	ArrayList alPoEmailType =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALPOEMAILTYPE"));
	ArrayList alGrpTypes =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALGRPTYPE"));
	ArrayList alAgingTyp =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALAGETYPE"));
	ArrayList alCompList =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOMPLIST"));
	ArrayList alCollectorList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOLLECTOR"));
	ArrayList alAccountList =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALACCLIST"));
	ArrayList alRepList =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALREPLIST"));
	ArrayList alParentList =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALPARENTLIST"));
	ArrayList alCompCurrency =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOMPANYCURRENCY"));
	String strExclCmpny =  GmCommonClass.parseNull((String)request.getAttribute("EXCLCOMPNY"));
	ArrayList alDistList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALDISTLIST"));
	int intLength = alReturn.size();

	HashMap hmLoop = new HashMap();

	String strAccId = "";
	String strAccName = "";
	String strOrdId = "";
	String strOrdDate = "";
	String strCSPerson = "";
	String strPrice = "";
	
	String strShade = "";
	String strWarningGif = "";
	String strParentOrdId = "";
	String strCustomerPO = "";
	String strOrdSumId = "";
	String strHeldFl = "";
	String strCollectorNm = "";
	String strOrdType = "";
	String strRepCols="1";
	String strPSlipLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_PACK_SLIP"));
	String strDOIdLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_DO_ID"));
	String strOrdDtLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_ORD_DATE"));
	String strGroupType = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_GRP_TYPE"));
	String strLabel2 = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_DISTRIBUTOR"));
	String strDeptId = (String)session.getAttribute("strSessDeptSeq")==null?"":(String)session.getAttribute("strSessDeptSeq");
	String strAccessLvl = (String)session.getAttribute("strSessAccLvl")==null?"":(String)session.getAttribute("strSessAccLvl");
	String strApplnDateFmt = strGCompDateFmt;
	String strCurrSign = GmCommonClass.parseNull((String)request.getAttribute("ARCURRSYMBOL"));
	//Below code is to track down the visits on the report.
	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
	String strUserName = strUserId + " - " + strShUserName;
	//Visit code ends
	String strOpt = GmCommonClass.parseNull((String)hmParam.get("OPT"));
	String strAccountCurrency = GmCommonClass.parseNull((String)request.getAttribute("ARCURRID"));
	String strAckAccountCurrency = "";
	
	if (strOpt.equals(""))
	{
		strOpt = GmCommonClass.parseNull((String)session.getAttribute("strSessOpt"));	
	}
	
	boolean bolEmail = false;
	String strJSFnNm = "fnViewOrder";
	if (strDeptId.equals("2020") || strDeptId.equals("2006") || strOpt.equals("PROFORMA"))
	{
		strJSFnNm = "fnCallPO";
		if (!strDeptId.equals("2001") && strOpt.equals("EMAIL"))
		{
			bolEmail = true;		
		}
	}
		
	String strType = GmCommonClass.parseNull((String)hmParam.get("TYPE"));
	java.util.Date dtFrom = (java.util.Date)hmParam.get("FRMDT");
	java.util.Date dtTo = (java.util.Date)hmParam.get("TODT");
	String strTodate =  GmCommonClass.parseNull((String)GmCommonClass.getStringFromDate(dtTo,strApplnDateFmt));
	String strMode = GmCommonClass.parseNull((String)hmParam.get("DTYPE")); 
	String strReportType = GmCommonClass.parseNull((String)hmParam.get("REPORTTYPE"));

	String strOrderType = GmCommonClass.parseNull((String)hmParam.get("ORDERTYPE"));
	String strCollectorId = GmCommonClass.parseNull((String)hmParam.get("COLLECTORID"));
	String strCompanyID = GmCommonClass.parseNull((String)hmParam.get("COMPANYID"));
	String strMasterID = GmCommonClass.parseNull((String)hmParam.get("MASTERID"));
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("strAction"));
	String strDueDays = GmCommonClass.parseNull((String)request.getAttribute("DEFAULTDUEDAYS"));
	//String strChkOrderType = GmCommonClass.parseNull((String)request.getAttribute("ORDERTYPE"));
	
	ArrayList alMasterTyp = new ArrayList();
	String strGraphTitle = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_PO_GRAPH_HEADER"));
	if (strOpt.equals("HELDRPT")){
		strGraphTitle = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_HELD_RPT_HEADER"));
	}else if (strOpt.equals("EMAIL")){
		strGraphTitle = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_EMAIL_REMIND_HEADER"));
		strWikiTitle = GmCommonClass.getWikiTitle("PO_EMAIL_REMAINDER");
	}else if (strOpt.equals("PROFORMA")){
		strGraphTitle = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_PROFORMA_ACK_HEADER"));
		strPSlipLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_ACK_ID"));
		strDOIdLabel =  GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_ACK_ORDER"));
		strOrdDtLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_ACK_DATE"));
		strGroupType =  GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_RPT_TYPE"));
		strLabel2 =     GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_FIELD_SALES"));
		strWikiTitle = GmCommonClass.getWikiTitle("ACKNOWLEDGEMENT_REPORT");
		// to get the Ack currency value
		strAckAccountCurrency = GmCommonClass.parseNull((String)request.getAttribute("ACKCURRSYM"));
	}
	String strAName = "";
	String strRName = "";
	String strHeader = "";
	String strLabel = "";
	String strRefId = "";
	String strRefNm = "";
	String strLblNm = "";
	String strDistNm = "DNAME";
	
	if (strReportType.equals("103284"))  //BYREP
	{
		strRefId = "RID";
		strRefNm = "RNAME";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_REP"));
		strLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_ACCOUNT"));
		strLblNm = "NAME";
		strJSFnNm = "fnViewOrder";
	}
	else if (strReportType.equals("103283"))  //BYACC
	{
		strRefId = "ACCID";
		strRefNm = "NAME";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_ACCOUNT"));
		strLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_REP"));
		strLblNm = "RNAME";
	}else if (strReportType.equals("103286"))  //BY Parent Account
	{
		strRefId = "ACCID";
		strRefNm = "NAME";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_PARENT_ACCOUNT"));
		strLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_REP"));
		strLblNm = "RNAME";
	}
	else
	{
		strRefId = "DID";
		strRefNm = "DNAME";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_DIST"));
		strLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_ACCOUNT"));
		strLblNm = "NAME";
		strDistNm = "RNAME";
		strLabel2 = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_REP"));
	}
	
	ArrayList alDist = new ArrayList();
	ArrayList alSales = new ArrayList();
	int intExclCmpnyFl;
	String strTempOrderId = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Pending Orders (PO) Report </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/sales/GmSalesFilter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Accounts/GmPendingPOEmail.js"></script>

<script>
var bolEmailFl = '<%=bolEmail%>';
var accArrLen ='<%=alAccountList.size()%>';
var distArrLen ='<%=alDistList.size()%>';
var repArrLen ='<%=alRepList.size()%>';
var parentAcctArrLen ='<%=alParentList.size()%>';
var selMasterID = '<%=strMasterID%>';
var format = '<%=strApplnDateFmt%>';
var arParentSelection =  new Array();
var masterSelID='<%=strMasterID%>';
var grpType = '<%=strReportType%>';
var compCurrSign = '<%=strAccountCurrency%>';
var ackCurr = '<%=strAckAccountCurrency%>';

<%	//Forming the Distributor Options List array.
  HashMap hcboVal = new HashMap();
	for (int i=0;i<alDistList.size();i++)
	{
		hcboVal = (HashMap)alDistList.get(i);
%>
	var DistArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%	} 
	//Forming the Accounts Options List array.
	hcboVal = new HashMap();
	for (int i=0;i<alAccountList.size();i++)
	{
		hcboVal = (HashMap)alAccountList.get(i);
%>
	var AccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
	
	//Forming the Sales Reps Options List array.
	hcboVal = new HashMap();
	for (int i=0;i<alRepList.size();i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
	var RepArr<%=i%> = "<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>



function fnGo()
{
	varFromDate = document.frmAccount.Txt_FromDate.value;
	varToDate = document.frmAccount.Txt_ToDate.value;
	varType = document.frmAccount.Cbo_Type.value;	

	if (varFromDate != '' && varToDate != '' && varType != '0')
	{
		Error_Details(message[10506]);
	}	
	if(varFromDate != ''){
		CommonDateValidation(document.frmAccount.Txt_FromDate,format,Error_Details_Trans(message[10507],format));
	}
	if(varToDate != ''){
		CommonDateValidation(document.frmAccount.Txt_ToDate,format,Error_Details_Trans(message[10508],format));
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	else {
		fnStartProgress("Y");// When click Go button, it will show Progress Bar.
		document.frmAccount.hType.value = document.frmAccount.Cbo_Type.value;
		document.frmAccount.hReportType.value = document.frmAccount.Cbo_ReportType.value;
		if( bolEmailFl != 'true'){
			fnReloadSales('Reload','<%=strServletPath%>/GmPendingOrdersPOServlet');
		}else{	
			document.frmAccount.hAction.value = "Reload";
			document.frmAccount.submit();
		}
	}
}

</script>
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPendingOrdersPOServlet">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hPgToLoad" value="GmOrderItemServlet">
<input type="hidden" name="hSubMenu" value="Trans">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hFrom" value="Dashboard">
<input type="hidden" name="hPO" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="<%=strOpt%>">
<input type="hidden" name="hToDate" value="<%=strTodate%>">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hType" value="<%=strType%>">
<input type="hidden" name="hOrdId" value="">
<input type="hidden" name="hReportType" value="<%=strReportType%>">
<input type="hidden" name="hDeptId" value="<%=strDeptId%>">
<input type="hidden" name="hordertype" value="<%=strOrderType%>">
<input type="hidden" name="hduedays" value="<%=strDueDays%>">

	<table border="0"  width="900" cellspacing="0" cellpadding="0">
		<tr><td colspan="6" bgcolor="#666666"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
			<td valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td height="25" class="RightDashBoardHeader" colspan="6"><%=strGraphTitle%></td>
						<fmtPORpt:message key="IMG_ALT_HELP" var="varHelp"/>
						<td align="right" class=RightDashBoardHeader><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
						title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
						</td>
					</tr>
					<% if (!bolEmail){  %>
					<tr>
						<td colspan="10">
						<jsp:include page="/sales/GmSalesFilters.jsp" >
							<jsp:param name="FRMNAME" value="frmAccount" />
							<jsp:param name="HACTION" value="Reload"/>
							<jsp:param name="HIDE" value="SYSTEM" />				
							<jsp:param name="HIDEBUTTON" value="HIDEGO" />
						</jsp:include>
						</td>
					</tr>
					<% } %>	
		<tr><td class="Line" colspan="7"></td></tr>
		<tr class="shade">
			<td height="20" class="RightText" colspan="7">&nbsp;&nbsp;<img src=images/dot_red.gif height=10 width=9> - <fmtPORpt:message key="TD_PO_ACTION_MSG"/></td>
		</tr>
		<tr><td class="LLine" colspan="7"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<fmtPORpt:message key="LBL_AGE_TYPE" var="varAgeType"/>
			<td class="RightTableCaption" align="right" height="25"><gmjsp:label type="RegularText"  SFLblControlName="${varAgeType}:" td="false"/></td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Type"  seletedValue="<%=strType%>"  defaultValue= "[Choose One]"	
			tabIndex="2"  value="<%=alAgingTyp%>" codeId="CODEID" codeName="CODENM"/>
			</td>
			<fmtPORpt:message key="LBL_DATE_RANGE" var="varDateRange"/>
			<td class="RightTableCaption" align="right" height="25"><gmjsp:label type="RegularText"  SFLblControlName="${varDateRange}:" td="false"/></td>
			<td colspan="6">
				<table border="0"  cellspacing="0" cellpadding="0">
				<tr>
				<td>&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(dtFrom==null)?null: new java.sql.Date(dtFrom.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
				<td>&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(dtTo==null)?null: new java.sql.Date(dtTo.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
				<fmtPORpt:message key="BTN_LOAD" var="varLoad"/>
				<td style="padding-left:4em;">&nbsp;&nbsp;<gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnGo();" tabindex="9"/></td>
				</tr>
				</table>
			</td>
		</tr>
		<tr><td class="LLine" colspan="7"></td></tr>
		<tr height="25" class="shade">
<%
	if (strDeptId.equals("2020") || strDeptId.equals("2006"))
	{
%>			<fmtPORpt:message key="LBL_DIST_TYPE" var="varDistType"/>		
			<td class="RightTableCaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varDistType}:" td="false"/></td>
			<td class="RightText">&nbsp;<gmjsp:dropdown controlName="Cbo_DType"  seletedValue="<%=strMode%>"  defaultValue= "[Choose One]"	
			tabIndex="-1"  value="<%=alDTypes%>" codeId="CODEID" codeName="CODENM"/></td>
<%
	}else if(!strOpt.equals("PROFORMA")){
		strRepCols = "3";
%>
			<td colspan="2">&nbsp;</td>
<%
	}
%>
						<td class="RightTableCaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="<%=strGroupType%>" td="false"/>:</td>
						<td class="RightText" colspan="<%=strRepCols%>">&nbsp;<gmjsp:dropdown controlName="Cbo_ReportType"  seletedValue="<%=strReportType%>" 
						 defaultValue="[Choose One]" tabIndex="2"  value="<%=alGrpTypes%>" codeId="CODEID" codeName="CODENM" onChange="fnLoadMasterInfo();"/>
						 &nbsp;<% if (bolEmail){  %>
						 <gmjsp:dropdown controlName="Cbo_Master"  seletedValue="<%=strMasterID%>"  defaultValue="[Choose One]" tabIndex="2" width="350" value="<%=alMasterTyp%>" codeId="CODEID" codeName="CODENM" onChange="fnARcurrency();"/>
						<% }  %>
<%--                          <font color="red" style="padding-left:4em;">*</font><b>Currency:</b>&nbsp;
			             <gmjsp:dropdown controlName="Cbo_Comp_Curr"  seletedValue="<%=strAccountCurrency%>" value="<%=alCompCurrency%>" codeId = "ID"  codeName = "NAME" />&nbsp;&nbsp;&nbsp; --%>
			             </td>
			             <%if(strOpt.equals("PROFORMA")){ %>
	                     <td class="RightTableCaption" align="right" height="25"><span id="spnCurrLbl" name="spnCurrLbl"><font style="padding-left:4em;"></font><b><fmtPORpt:message key="LBL_CURRENCY" />:</b></span></td>
	                     <td colspan="6">&nbsp;<gmjsp:dropdown controlName="Cbo_Comp_Curr_ack"  defaultValue= "[All]"  seletedValue="<%=strAckAccountCurrency%>" value="<%=alCompCurrency%>" codeId = "ID"  codeName = "NAMEALT" optionId="NAME" />&nbsp;&nbsp;&nbsp;</td>

		                <% } else{
%>
			<td colspan="3"></td>
<%
	}
%> 
			             
						</tr>
						<%if(!strOpt.equals("PROFORMA")){ %>
						<tr><td class="LLine" colspan="7"></td></tr>
						<tr height="25">
						<fmtPORpt:message key="LBL_COLLECTER_ID" var="varCollecterId"/>
						<td class="RightTableCaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varCollecterId}:" td="false"/></td>
						<td class="RightText">&nbsp;<gmjsp:dropdown controlName="Cbo_CollectorId" seletedValue="<%=strCollectorId%>"   defaultValue= "[Choose One]"	
			            tabIndex="-1"  value="<%=alCollectorList%>" codeId="CODEID" codeName="CODENM"/></td>
			            <fmtPORpt:message key="LBL_RPT_TYPE" var="varReportType"/>
						<td class="RightTableCaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varReportType}:" td="false"/></td>
						<td class="RightText">&nbsp;<gmjsp:dropdown controlName="Cbo_OType" controlId="Cbo_OType" seletedValue="<%=strOrderType%>"  defaultValue= "[Choose One]"	
			            tabIndex="-1"  value="<%=alOTypes%>" codeId="CODEID" codeName="CODENM"/></td>
			            
			            
			            <td class="RightText"><span id="spnCurrLbl" name="spnCurrLbl"><font " style="padding-left:4em;"></font><b><fmtPORpt:message key="LBL_CURRENCY" />:</b>&nbsp;
			            </span>
			            <gmjsp:dropdown controlName="Cbo_Comp_Curr"  seletedValue="<%=strAccountCurrency%>" value="<%=alCompCurrency%>" codeId = "ID"  codeName = "NAMEALT" optionId="NAME" />&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<%} %>
					<% if (bolEmail){  %>
					<tr><td class="LLine" colspan="7"></td></tr>
					<tr height="25" class="shade" colspan="10">
						<fmtPORpt:message key="LBL_COMPANY" var="varCompany"/>
						<td class="RightTableCaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varCompany}:" td="false"/></td>
						<td class="RightText">&nbsp;<gmjsp:dropdown controlName="Cbo_CompanyID" seletedValue="<%=strCompanyID%>"   defaultValue= "[Choose One]"	
			            tabIndex="-1"  value="<%=alCompList%>" codeId="CODEID" codeName="CODENM"/></td>
						<!-- <td class="RightTableCaption" align="right" colspan="0">&nbsp; </td> -->
						 <fmtPORpt:message key="LBL_NUM_DAYS" var="varNumDays"/>			
						 <gmjsp:label type="MandatoryText"  SFLblControlName="${varNumDays}:" td="true"/>
						<td class="shade" colspan="7">&nbsp;<input maxlength='2'   type="text" size="5" value="<%=strDueDays%>" name="Txt_DueDate" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td>
					</tr>
					<% }  %>
				</table>
			</td>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
		</tr>
		<tr>
			<td>
				<table border="0"  width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="Line" colspan="12"></td></tr>
					<%if( (strOpt.equals("EMAIL") && strAction.equals("Reload")) ||  (!strOpt.equals("EMAIL"))){ %>
					<tr class="ShadeRightTableCaption" colspan="10">
						<td HEIGHT="24" width="70"><%=strHeader%></td>
						<td width="100"><%=strPSlipLabel%></td>
						<td width="100"><%=strDOIdLabel%></td>
						<td width="150"><fmtPORpt:message key="LBL_CUST_PO"/></td>
						<td width="120"><fmtPORpt:message key="LBL_COLLECTER_ID"/></td>
						<td width="250"><%=strLabel%></td>
						<td width="100" align="center"><%=strOrdDtLabel%></td>
						<td width="100" align="center"><%=strLabel2%></td>
						<td width="100" align="center"><fmtPORpt:message key="LBL_AMOUNT"/></td>
					</tr>
<%
						if (intLength > 0)
						{
							HashMap hmTempLoop = new HashMap();

							String strNextId = "";
							int intCount = 0;
							String strLine = "";
							String strTotal = "";
							String strAcctTotal = "";
							String strChildOdrFl = "";
							boolean blFlag = false;
							boolean blFlag1 = false;
							double dbSales = 0.0;								
							double dbAcctTotal = 0.0;
							double dbTotal = 0.0;

							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alReturn.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get(strRefId));
								}

								strAccId = GmCommonClass.parseNull((String)hmLoop.get(strRefId));
								strAccName = GmCommonClass.parseNull((String)hmLoop.get(strRefNm));
								strOrdId = GmCommonClass.parseNull((String)hmLoop.get("ORDID"));
								strOrdType = GmCommonClass.parseNull((String)hmLoop.get("ORDTYPE"));								
								strOrdDate = GmCommonClass.getStringFromDate((java.sql.Date)hmLoop.get("ORDDT"),strGCompDateFmt);
								strPrice = GmCommonClass.parseZero((String)hmLoop.get("COST"));
								strCSPerson = GmCommonClass.parseZero((String)hmLoop.get(strDistNm));
								strWarningGif = GmCommonClass.parseZero((String)hmLoop.get("FL"));
								strWarningGif = strWarningGif.equals("Y")?"<img src=images/dot_red.gif height=10 width=9>":"";
								strAName = GmCommonClass.parseNull((String)hmLoop.get(strLblNm));
								strParentOrdId = GmCommonClass.parseNull((String)hmLoop.get("PARENTORDID"));
								strCustomerPO = GmCommonClass.parseNull((String)hmLoop.get("CUSTOMERPO"));
								strOrdSumId = strParentOrdId.equals("")?strOrdId:strParentOrdId;
								strHeldFl = GmCommonClass.parseNull((String)hmLoop.get("HOLD_FL"));
								strCollectorNm = GmCommonClass.parseNull((String)hmLoop.get("COLLECTOR_NAME"));
								strChildOdrFl = GmCommonClass.parseNull((String)hmLoop.get("CHILD_ORDER_FL"));
								if(strOpt.equals("PROFORMA")){
									strCurrSign = GmCommonClass.parseZero((String)hmLoop.get("CURRSYMBOL"));
								}
								
								dbSales = Double.parseDouble(strPrice);
								dbAcctTotal = dbAcctTotal + dbSales;
								dbTotal = dbTotal + dbSales;
								if (strAccId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=11 height=1 class=LLine></TD></TR>";
								}
								else
								{
									blFlag1 = true;
									strLine = "<TR><TD colspan=11 height=1 class=LLine></TD></TR>";
									if (intCount > 0)
									{
										strAccName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
									//
								}
								strAcctTotal = ""+dbAcctTotal;

								if (intCount > 1)
								{
									strAccName = "";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

								out.print(strLine);
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
								alDist.add(strAccName);
								
								intExclCmpnyFl = strOrdId.indexOf(strExclCmpny);
								// Remove the company id append with the order id
								if(intExclCmpnyFl != -1){
								  strTempOrderId = strOrdId.replace("-"+strExclCmpny, "");
								}else{
								  strTempOrderId = strOrdId;
								}
%>
					<tr id="tr<%=strAccId%>">
						<td colspan="6" height="20" class="RightText">&nbsp;
						<fmtPORpt:message key="IMG_ALT_EXPAND" var="varExpand"/>
						<fmtPORpt:message key="IMG_ALT_PACKING_SLIP" var="varPackingSlip"/>
						<fmtPORpt:message key="IMG_ALT_PACK_SLIP" var="varPackSlip"/>
						<fmtPORpt:message key="IMG_ALT_DO_HOLD" var="varDoHold"/>
						<fmtPORpt:message key="IMG_ALT_ORD_SUMMARY" var="varOrdSummary"/>
						<% if (bolEmail){  %>
							&nbsp;<input type="checkbox" name="parentsel<%=strAccId%>" id="<%=strAccId%>" onclick="javascript:fnSelectOrders('<%=strAccId%>');" />
						<% }  %><a class="RightText" title="${varExpand}" href="javascript:Toggle('<%=strAccId%>')"><%=strAccName%></a>
						<input type="hidden" name="hAccIdVal" value="<%=strAccId%>" id="<%=strAccId%>"/>				
						
						</td>
						<td colspan="10" class="RightText" id="td<%=strAccId%>" align="right"></td>
					</tr>
					<tr>
						<td colspan="10"><div style="display:none" id="div<%=strAccId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strAccId%>">
<%
								blFlag = false;
								}
%>
								<tr <%=strShade%>>
									<td align="right" height="20" nowrap>&nbsp;
<%						// Check if the Orded is a Return or Duplicate Order -- redirect based on the type 						
						if ((strTempOrderId.charAt(strTempOrderId.length()-1)== 'R') ||(strTempOrderId.charAt(strTempOrderId.length()-1)== 'D') )
						{%><a href="javascript:fnPrintCreditMemo('<%=strOrdId%>');" 
<%						}else if (strTempOrderId.charAt(strTempOrderId.length()-1)== 'A') 
						{%><a href="javascript:fnPrintCashAdjustMemo('<%=strOrdId%>');" 
<%						}else
						{%>	<a href="javascript:fnPrintPick('<%=strOrdId%>','<%=strOrdType %>');" title="${varPackingSlip}" 
<%						}%>								
						class="RightText"><img src="<%=strImagePath%>/packslip.gif" title="${varPackSlip}"  border=0></a>
						<!-- AR-23 - When hold flag is Y , it will be displayed Hold Icon image-->
						 <%if (strHeldFl.equals("Y")){ %> 
						<img src="<%=strImagePath%>/hold-icon.png" height="15" title="${varDoHold}"  border=0></a>
						 <%} %> 
						<a href="javascript:fnViewOrder('<%=strOrdSumId%>');"><img src="<%=strImagePath%>/ordsum.gif" title="${varOrdSummary}"  border=0></a>
<%
	if (bolEmail){
%>
									<input type="checkbox" name="Chk_Id<%=strAccId%>" value="<%=strOrdId%>" id="<%=strAccId%>"/>					
									<input type="hidden" name="h<%=strOrdId.replaceAll("-","")%>chldfl" value="<%=strChildOdrFl%>" id="<%=strOrdId%>">
<%
	}
%>									
									<%=strWarningGif%></td>
									<td width="100" class="RightText">&nbsp;<a class=RightText href= javascript:<%=strJSFnNm%>('<%=strOrdId%>')><%=strOrdId%></a></td>
									<td width="100" class="RightText">&nbsp;<%=strParentOrdId%></td>
									<td width="150" class="RightText">&nbsp;<%=strCustomerPO%></td>
									<td width="120" class="RightText">&nbsp;<%=strCollectorNm%></td>
									<td width="250" class="RightText"><%=strAName%></td>
									<td width="100" class="RightText"><%=strOrdDate%></td>
									<td width="100" class="RightText">&nbsp;<%=strCSPerson%></td>
									<gmjsp:currency type="CurrTextSign"  textValue="<%=strPrice%>" currSymbol="<%=strCurrSign %>"/><%--<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice))%> --%>
								</tr>
					<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
<%					
					if ( intCount == 0 || i == intLength-1)
					{
					alSales.add(strAcctTotal);
%>
							</table></div>
						</td>
					</tr>
					<script>
						document.all.td<%=strAccId%>.innerHTML ="<%=strCurrSign%><%=GmCommonClass.getStringWithCommas(strAcctTotal,(String)session.getAttribute("strSessApplCurrFmt"))%>"+"&nbsp;"; //$<%=GmCommonClass.getStringWithCommas(strAcctTotal)%>
					</script>
<%
						dbAcctTotal = 0.0;					
					}
				strTotal = ""+dbTotal;
				}// end of FOR
%>
				<% if(!strAckAccountCurrency.equals("0")){%>	
					<tr><td colspan="9" class="Line"></td></tr>
					<tr>
						<td align="right"  class="RightTableCaption" height="20" colspan="8"><fmtPORpt:message key="LBL_GRAND_TOTAL"/>&nbsp;</td>
						<gmjsp:currency type="CurrTextSign"  textValue="<%=strTotal%>" currSymbol="<%=strCurrSign %>"/><%--$<%=GmCommonClass.getStringWithCommas(strTotal)%>--%>
					<% }%>
					</tr>
					<tr><td colspan="9" class="Line"></td></tr>							
<%
						} // End of IF
						else{
%>
					<tr><td colspan="9" class="Line"></td></tr>					
					<tr><td colspan="9" class="RightTextRed" height="50" align=center><fmtPORpt:message key="TD_NO_ORD_PEND_PO"/></td></tr>
					<tr><td colspan="9" class="Line"></td></tr>	
<%					
						}
%>
<%
	if (bolEmail && strAction.equals("Reload") && (intLength > 0) ){
%>
					<tr height="25" class="shade">
						 <td colspan="9" height="30" align ="center" class="RightTableCaption">&nbsp;&nbsp;<fmtPORpt:message key="LBL_CHOOSE_ACTION"/>:&nbsp; 
							<gmjsp:dropdown controlName="Cbo_EmailAction" defaultValue= "[Choose One]" tabIndex="-1" value="<%=alPoEmailType%>" codeId="CODEID" codeName="CODENM"/>
					        <fmtPORpt:message key="BTN_SUBMIT" var="varSubmit"/>
					        <gmjsp:button name="Submit"  value="&nbsp;&nbsp;${varSubmit}&nbsp;&nbsp;" buttonType="Save" onClick="fnSubmitAction();"/>&nbsp;&nbsp;
					     </td>
					</tr>
					<tr><td colspan="9" class="Line"></td></tr>	
<%
	}
}
%>
				</table>
			</td>
		</tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
